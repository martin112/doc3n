$in:rst Doc3n Issues

improvement: generate table of content
--------------------------------------

A way of showing available content (Table Of Content) is definitely needed.

It needs to handle single document case (usually one toc), and multi pages
with one global toc and/or a toc on each page.

Idea
====

Put an $include:toc [\d] after a header to generate it after any text in that
section::

  Table Of Content
  ----------------
  $toc:[param value]

The toc is from the parent level of this header::

  Table of Content
  Parent header
  .. headers before
  .... sub headers
  .. table of content
  .. headers after
  ....  sub header

It should also be possible to include a toc from build script::

  pages.generate_toc({param:value})  # for all pages
  page.generate_toc({param:value})  # for this and included pages

Settings could be made either from building process.

Each entry links to it's header.

Add a class to each level. ("doc3n_toc1" "doc3n_toc2"...)

Things that make this hard
==========================

Single page documents include other documents on need. This is not known
beforehand, and could cause page/s.generate_toc to be problematic. If possible
to determine this f.ex. in to_page-script it would be easier.

There are probably a lot of settings that someone may want. What are they,
and how and where to express them?

* depth - subheaders to include f.ex. $doc:depth 3 (including toc header level)
* prefix - css stylable (needs to mark toc and headers with classes)

Line numbering is currently very hard, and for most output formats, not
applicable. It could maybe to some extent be solved with Pandoc.
