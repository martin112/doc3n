$in:rst Doc3n Issues

issue: flat html
----------------

This is an issue with the pandoc html5 template.

Check if posible to generate sections:

.. code:: html

    <body>
        <section>
            <h1>
            <p>
            <p>
            <section>
                <h2>
                <p>
        </section>

or

.. code:: html

    <body>
        <section class="h1">
            <h1>
            <p>
            <p>
        </section>
        <section class="h2">
            <h2>
            <p>
