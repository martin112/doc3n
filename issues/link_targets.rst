$in:rst Doc3n Issues


Suggestion: option to map links (specially faulty ones)
-------------------------------------------------------

Some links are to separate projects but not fully qualified
urls. These projects could maybe move around sometimes.

A suggestion is to add an input dict with link replacements::

    map_links: {<link target, f.ex. header>: <custom location>}
