$in:rst Doc3n Issues

issue: include documents
------------------------

Generating single page documents sometimes require inclusion
of other documents.

An idea is to add some kind of tag right under a section, doing this.

Suggested syntax::

  | $doc:rst Some Document
  |
  | $doc:rst Some Other
  | $include: Some Document
  |
  | $in:rst Some Other
  | $include: Some Document

Rendering will depend on export method.

Single page could include the document, maybe a link if it is already done once.

Multipage rendering could create a link to the document.

