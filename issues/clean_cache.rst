$in:rst Doc3n Issues

issue: Clean cache
------------------

The new gather should have some kind of cache cleanup
for old files not included anymore.

The standard version would be to clean up all files
not touched "this" update (timestamp older than start).

But it has to have an option to specify the timestamp,
in case of multipe, separate instances doing gathering
of different subdata.
