#!/usr/bin/python3
"""
$doc:rst Doc3n Writing Documentation

Fragments
=========

The whole intention with Doc3n_ is to spread out the documentation in fragments
in comments in the source code, as close to relevant methods and definitions as possible.

This intends to keep the documentation up to date. It is also possible to
include links in the documents back directly to the source code so it is easier
to understand the code base.

Thanks to the backend, Pandoc_, it is possible to write and mix most markup formats,
and export documents in the format you need.

Documentation header
====================

Links and Images
================

Source code formats
===================

Different file formats have different ways of defining comments and
detection is a bit fuzzy. Doc3n_ aims to support the most common cases
for the most standard formats, like c-style, xml-comments and python
triple quotes.

Supported formats
-----------------

Indentation
===========

Actual content
==============

The hardest part may be what to actually write and to do it.

Bad documentation can be worse than none if it lures the reader
into missunderstanding or wrong prejudices.

Answer questions
----------------

These ones is a good start:

* Intention of project
* How to use it
* How to set it up
* How to extend and improve the project

Keep facts close to definitions
-------------------------------

If you specify how to use a method, write it close to that method
so if it is updated, the documentation has a higher chance to be
it too.

Disposition
-----------

It can be helpfull to write a summary of what headers you need
and how they should be ordered in a hierarchy in a single page.
This way it is easier to know what headers to use and what to
concentrate on.

Write it
--------

Actually start writing it. Write about the concept or the most
important details. After that you can fill in on everything
in between and update headers to make for easy reading.

"""
# pylint: disable=pointless-string-statement

import re
import logging
from pathlib import Path
from typing import Dict, Tuple, Iterator, List, Optional, Union, TYPE_CHECKING

if TYPE_CHECKING:
    from .cache import Cache

from .extractor import ExtractorComments, ExtractorFullText, extract_comments

LOG_FRAGMENT = logging.getLogger("doc3n.find.fragment")
FRAGMENT_VERSION = "0.1"  # for making sure cache isn't old

TYPE_TAGS = Dict[str, List[str]]


class Source:
    "Reference to a source file"

    def __init__(self, cache: 'Cache', file_path: Path, line_nr: int, mime: str):
        self.cache = cache
        self.file_path = file_path
        self.line_nr = line_nr
        self.mime = mime

    def __str__(self) -> str:
        return r"{0}#{1}".format(self.file_path, self.line_nr)


class Fragment:
    """$in:rst Glossary
    Fragment
    ========
    Contains one section of unprocessed markup and meta data about where it
    comes from, where it should be placed and markup language.
    """

    version = FRAGMENT_VERSION

    def __init__(
        self,
        markup: str,
        text: str,
        tags: TYPE_TAGS,
        main_header: str,
        is_root_doc: bool,
        source: Optional[Source] = None,
    ):
        self.main_header_str = main_header
        self.is_root_doc = is_root_doc
        self.tags: TYPE_TAGS = tags
        self.source = source
        self.markup = markup
        self.content = text
        if self.source:
            LOG_FRAGMENT.debug(
                "%s %s in %s#%s",
                "doc" if is_root_doc else "header",
                self.main_header_str,
                self.source.file_path,
                self.source.line_nr,
            )

    def version_ok(self) -> bool:
        "Used to verify version in cache"
        if self.version != FRAGMENT_VERSION:
            LOG_FRAGMENT.warning(
                "Fragment version %s missmatch current version %s",
                self.version,
                FRAGMENT_VERSION,
            )
            return False
        return True

    def __str__(self) -> str:
        if len(self.content) > 50:
            content = '"{start}..{length}..{end}"'.format(
                start=self.content[:30].replace("\n", "\\"),
                end=self.content[-30:].replace("\n", "\\"),
                length=len(self.content),
            )
        else:
            content = self.content.replace("\n", " ")
        return "${method}:{markup} {header} <{path} {type}> {content}".format(
            method="doc" if self.is_root_doc else "in",
            markup=self.markup,
            header=self.main_header_str,
            path=str(self.source) if self.source else ".",
            type=self.source.mime if self.source else "",
            content=content,
        )


"""
$in:rst Documentation header

Each documentation fragment starts with a specific header. A fragment can 
either be made to be placed under another header specified somewhere else,
or be used for creating a new separate document (they can be joined later
when exporting though).

The header is written by itself on a line, or at the start of a comment.

.. code::

    ${type}:{markup language} {header}

:type: Defines if this is a document or should be placed under another section
:markup language: what markup this section is written in
:header: either document title or what header to place this section under, depending on type

Some examples:

* ``$doc:rst My Own Document``
* ``$in:md My Own Document``
* ``$in:html5 Any other section``

There may be different documentation fragments in the same comment, with different markup language.

Markup language
===============
The best is to read about the different languages and see what suits you. But remember that
you can always mix them in Doc3n:

:rst: `Restructured text <https://en.wikipedia.org/wiki/ReStructuredText>`_ Is quite powerful,
      especially with links. Used in f.ex. Sphinx python documentation.

:md: `Markdown <https://en.wikipedia.org/wiki/Markdown>`_ a bit more basic than rst, popular
     from may repository sites like f.ex. github.

Restructured Text
~~~~~~~~~~~~~~~~~
This documentation is written in restructuredtext so it is described a tiny bit here.

See specification: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html.

Headers are in restructuredtext writen with a underline, each new header style is placed one level
lower (For Doc3n, this is only per documentation block)::

    header name
    -----------

Suitable characters to use are the ones not ending comments in the specific file type
(e.g. '--' for xml or ', " for python)::

    = - ` : ~ ^ _ * + # < > " '

References should work, even to headers. So you can for example refer to
`Extract Documentation Fragments`_.

$in:rst links and images

Links to headers
================

You can add links to any header, including document headers. Because the document is
split up in fragments, all links have to be rebuild and Doc3n_ has to guess where they
lead. Therefore they may point wrong if there are missing headers or ambigious headers
with the same name.

If a header is missing, a warning is written to the terminal.

Images
......

Images can be included either |bmp_inline| or on a new line.

Threat image links just as `external links`_.

.. image:: $file:resources/free-file-icons/512px/bmp.png
    :height: 24pt

.. |bmp_inline| image:: $file:resources/free-file-icons/512px/bmp.png
    :height: 16pt

"""

DOC_HEADER = re.compile(
    r"(\n|^)\s*\$(?P<doc>in|doc):(?P<format>\w+) *(?P<header>[^\n]*)"
)


tagsType = Dict[str, List[str]]


class HeaderMatch:
    def __init__(
        self,
        header_start: int,
        header_end: int,
        doc: str,
        format: str,
        header: str,
    ):
        self.header_start = header_start
        self.header_end = header_end
        self.doc = doc
        self.format = format
        self.header = header
        self.tags: tagsType = {}


def extract_fragments_from_one_comment(
    comment_string: str,
    line_nr_start: int,
    mime: str,
    cache: 'Cache',
    file_path: Optional[Path] = None,
    source_prefix: Optional[Path] = None,
) -> Iterator[Fragment]:
    """Get all documentation fragments from one comment block.

    Search for headers, parse them and return them together with
    cut out content text (text between headers).
    """
    line_nr = line_nr_start
    header_end = 0
    content_end: int
    # information to return from previous search match in each loop
    last_match: Optional[HeaderMatch] = None  # doc/in, text_format, header
    match: Optional[HeaderMatch] = None
    while True:
        last_match = match
        content_end, match = extract_header(comment_string, header_end)
        if last_match:  # yield last found fragment
            if not (last_match.doc and last_match.format and last_match.header):
                header = comment_string[
                    last_match.header_start : last_match.header_end
                ].strip()
                LOG_FRAGMENT.warning(
                    'Skip invalid header "%s" in "%s#%d"', header, file_path, line_nr
                )
            else:
                source: Optional[Source] = None
                if file_path:
                    source_path = file_path.joinpath(source_prefix or "")
                    source = Source(
                        cache, file_path=source_path, mime=mime, line_nr=line_nr
                    )
                yield Fragment(
                    markup=last_match.format,
                    main_header=last_match.header,
                    is_root_doc=last_match.doc == "doc",
                    tags=last_match.tags,
                    text=comment_string[header_end:content_end],
                    source=source,
                )
            last_match = None
        if not match:
            break
        line_nr += (
            comment_string.count("\n", header_end, content_end) + 1
        )  # 1 for header
        header_end = match.header_end
        header_end, match.tags = extract_tags(comment_string, header_end)
        line_nr += len(match.tags)  # 1 for each tag


def extract_fragments(
    extractor: Union[ExtractorComments, ExtractorFullText],
    content: str,
    mime: str,
    cache: 'Cache',
    file_path: Optional[Path] = None,
    source_prefix: Optional[Path] = None,
) -> Iterator[Fragment]:
    """Find all comments from a text string and get all document fragments from it"""
    for comment, line_nr in extract_comments(extractor, mime, content):
        yield from extract_fragments_from_one_comment(
            comment, line_nr, mime, cache, file_path, source_prefix
        )


def extract_header(
    comment_string: str, last_header_end: int
) -> Tuple[int, Union[HeaderMatch, None]]:
    """Find and parse one document fragment header (e.g. $...)"""
    match = DOC_HEADER.search(comment_string, last_header_end)
    if match:
        return match.start(), HeaderMatch(
            header_start=match.start(),
            header_end=match.end() + 1,
            doc=match.group("doc"),
            format=match.group("format"),
            header=match.group("header").strip(),
        )
    return len(comment_string), None


"""
$in:rst Documentation header

Tags
====
Directly after a header, tags can be added, one per line. The tag is one of the
defined below. Tags may behave differently depending on formats and exporters.::

    ${tag name}:[optional value]

"""
DOC_TAG = re.compile(r"\$(?P<tag>[\w_-]+):(?P<val>[^\n]*)")


def extract_tags(comment_string: str, header_end: int) -> Tuple[int, tagsType]:
    """Find and parse tags, directly following header."""
    tags: tagsType = {}
    while True:
        match = DOC_TAG.match(comment_string, header_end)
        if match:
            tags.setdefault(match.group("tag"), []).append(match.group("val"))
            header_end = match.end() + 1
        else:
            break
    return header_end, tags


def test() -> None:
    """Some basic tests of this module"""
    # Hm... Any good idea


if __name__ == "__main__":
    test()
