#!/usr/bin/python3
from time import time
from shutil import rmtree, move
import sys
import os
from pathlib import Path
import pickle
from hashlib import md5
import logging
from typing import Any, List, Iterable, Final

from .fragment import Fragment

LOG_CACHE = logging.getLogger("doc3n.find.cache")
LOG_CACHEFILE = logging.getLogger("doc3n.find.cachefile")

CACHE_FOLDER_DEFAULT = '.documentation_cache'
CACHE_FOLDER_OLD = 'old'
CACHE_FOLDER_FILE = "doc3n_cache_folder"
CACHE_FOLDER_VERSION = "4"
CACHE_RESET_ENV = "DOC_CLEAR_CACHE"
CACHE_DISABLE = "DOC_NO_CACHE"


class VersionException(Exception):
    pass


"""
$in:rst Cache

Fragments_ are by default stored in a cache, both to improve speed and to
allow to spread information over multiple places and build in one place,
(see transfere cache).

Different caches are by default placed under the ``cache_folder``, and can be
used as contexts to manage gathering and exporting separately for different
parts of the documentation. Don't place other things in these folder, as it
may be overwritten or confuse doc3n.

The cache works by hashing the path to a file, check if that hash exists in the
context, and in that case, compare the cached timestamp with the file, to
see if it has been changed since last run.

Also text added through ``add_text`` is cached, even if never compared.

Transfere cache
---------------
Each cache is independent, and it's folder can be transfered to another system to
build the documentation there. Links will be resolved first when
building the documentation, so they can go between different cache contexts.
"""


def __get_full_path(path: str) -> Path:
    if path.startswith('/'):
        full_path = Path(path)
    elif '/' in path:
        full_path = Path(sys.argv[0]).absolute().parent.joinpath(path)
    else:
        full_path = Path(sys.argv[0]).absolute().parent.joinpath(CACHE_FOLDER_DEFAULT).joinpath(path)
    return full_path


def get_cache(path: 'str|Cache', remove_old_cached_files: bool, clear_cache=False) -> 'Cache|NoCache':
    if isinstance(path, Cache):
        return path
    if bool(os.environ.get(CACHE_DISABLE)):
        return NoCache()
    full_path = __get_full_path(path)
    cache = Cache(full_path, clear_cache)
    if remove_old_cached_files:
        cache.remove_old_cached_files()
    return cache

def get_no_cache() -> 'NoCache':
    return NoCache()


class Cache:
    def __init__(self, full_path: Path, clear_cache=False):
        self.cache_path: Final = full_path
        self.name: Final = '/'.join(full_path.parts[-2:])
        self.__remove_old = False  # see handle_removal_of_old_files
        """
        Use getCache instead

        Make sure it is a valid folder and that we have write access
        And clear it if requested

        Cache folders are
        .../{parent_cache_folder}/
            doc3n_cache_folder
            {cache_folder}

        {parent_cache_folder} defaults to ".documentation_cache" under the same folder as the run script lies.

        Cache can be initialized with a name for the cache_folder, or a file path.
        For file paths, they must include the parent_cache_folder. They may be relative to
        the script folder or absolute.
        """
        # Fix parent folder
        cache_parent_path = self.cache_path.parent
        if not cache_parent_path.exists():
            LOG_CACHE.info("Creating parent cache folder %s", cache_parent_path)
            # create cache folder and add cache folder version file
            cache_parent_path.mkdir()
            cache_folder_file = cache_parent_path.joinpath(CACHE_FOLDER_FILE)
            open(cache_folder_file, "w").write(CACHE_FOLDER_VERSION)
            self.cache_path.mkdir()
        else:
            # check cache folder version file
            cache_folder_file = cache_parent_path.joinpath(CACHE_FOLDER_FILE)
            if not cache_folder_file.is_file():
                raise Exception(
                    "%s is not a cache folder, correct the path or remove the folder" % cache_parent_path)
            elif cache_folder_file.read_text().rstrip() != CACHE_FOLDER_VERSION:
                raise VersionException(
                    f"Wrong cache folder version ({cache_folder_file.read_text()}) instead of "
                    f"{CACHE_FOLDER_VERSION}. Please remove folder {path} manually "
                    f"or run once with environment {CACHE_RESET_ENV}=1"
                )

        if not self.cache_path.exists():
            LOG_CACHE.info("Creating cache folder %s", self.cache_path)
            self.cache_path.mkdir()
        else:
            if bool(os.environ.get(CACHE_RESET_ENV)) or clear_cache:
                LOG_CACHE.info("Clearing cache folder %s", self.cache_path)
                rmtree(self.cache_path)
                LOG_CACHE.info("Creating cache folder %s", self.cache_path)
                self.cache_path.mkdir()
            else:
                LOG_CACHE.debug("Using cache %s", self.cache_path)

        if not os.access(self.cache_path, os.W_OK):
            raise Exception(
                "No write access to %s, fix access rights or select another cache folder"
                % self.cache_path
            )

    def remove_old_cached_files(self):
        """Clean up old removed cached files
        $in:rst Cache
        Each cache is cleaned up from old (removed) files at each run.
        A temporary folder, "old", is created under the cache folder for this.

        It is initiated by running ``{cache}.remove_old_cached_files()``.

        Note, run this before caching anything, and only once per each cache folder
        during a run, to not break the cache.
        """
        self.__remove_old = True
        old_cache = self.cache_path.joinpath(CACHE_FOLDER_OLD)
        if old_cache.exists():
            # count cache files (not cached texts)
            LOG_CACHE.debug('Cleaned up %d old files',
                len([f for f in old_cache.iterdir() if not f.name.startswith('text_')]))
            rmtree(old_cache)
        old_cache.mkdir()
        for entry in self.cache_path.iterdir():
            if not entry.is_dir():
                move(str(entry), str(old_cache))

    @staticmethod
    def __load_file(cache_file_path: Path) -> List["Fragment"]:
        fragments = pickle.load(open(cache_file_path, "rb"))
        if isinstance(fragments, list):
            f: Any
            for f in fragments:
                if not isinstance(f, Fragment):
                    break
                if not f.version_ok():
                    break
            else:
                return fragments
        raise VersionException("Cannot read fragments from %s", cache_file_path)


    def read_cached_file(self, file_path: Path) -> List["Fragment"]:
        "Check if file exists in cache, else raise LookupError"
        if self.__remove_old:
            cache_file_old_path = self.__hash_file_path(file_path, old=True)
            cache_file_new_path = self.__hash_file_path(file_path, old=False)
            file_mtime = file_path.stat().st_mtime
            if cache_file_new_path.exists():
                # should not occur in normal cases TODO investigate what could happen
                LOG_CACHEFILE.warning('Reading a cached file twice is uncommon, maybe you scan %s twice', file_path)
                return self.__load_file(cache_file_new_path)
            elif cache_file_old_path.exists():
                hash_mtime = cache_file_old_path.stat().st_mtime
                if file_mtime <= hash_mtime:
                    move(cache_file_old_path, cache_file_new_path)
                    return self.__load_file(cache_file_new_path)
            raise LookupError
        else:
            cache_file_path = self.__hash_file_path(file_path, old=False)
            file_mtime = file_path.stat().st_mtime
            if cache_file_path.exists():
                hash_mtime = cache_file_path.stat().st_mtime
                if file_mtime <= hash_mtime:
                    return self.__load_file(cache_file_path)
            raise LookupError


    def read_fragments(self) -> Iterable["Fragment"]:
        "Read all entries in this cache"
        count = 0
        for hash_file_path in self.cache_path.iterdir():
            if hash_file_path.name.startswith('.') or hash_file_path.is_dir():
                continue
            count += 1
            yield from self.__load_file(hash_file_path)
        LOG_CACHE.debug('Read %d fragments from %s', count, self.name)

    def write_text_to_cache(self, cache_id: str, content: List["Fragment"]) -> None:
        """Store text content as pickled data under cache/cache_id"""
        cache_file_path = self.cache_path.joinpath(cache_id)
        pickle.dump(content, open(cache_file_path, "wb"))
        os.utime(cache_file_path, (time(), time()))


    def write_file_to_cache(self, file: Path, content: List["Fragment"]) -> None:
        "Store content as pickled data under cache, in files named after their hashed path"
        cache_file_path = self.__hash_file_path(file, old=False)
        LOG_CACHEFILE.debug('cached %s %s', self.name, file)
        file_mtime = file.stat().st_mtime
        pickle.dump(content, open(cache_file_path, "wb"))
        os.utime(cache_file_path, (file_mtime, file_mtime))


    def __hash_file_path(self, file_path: Path, old: bool) -> Path:
        "hashed file path used in the cache"
        file_hash = md5(str(file_path).encode("utf-8")).hexdigest()
        if old:
            return self.cache_path.joinpath('old', file_hash)
        else:
            return self.cache_path.joinpath(file_hash)


class NoCache(Cache):
    """Use to disable cache"""
    def __init__(self):
        self.name='_NoCache_'

    def read_cached_file(self, file_path: Path) -> List["Fragment"]:
        raise LookupError

    def write_text_to_cache(self, cache_id: str, content: List["Fragment"]) -> None:
        return

    def write_file_to_cache(self, file: Path, content: List["Fragment"]) -> None:
        return


#####################


if __name__ == "__main__":
    # some test cases
    # run as python3 -m gather.cache
    logging.basicConfig(level=logging.DEBUG)
    cache = get_cache('test_cache', remove_old_cached_files=True)
    path = Path(__file__)
    from .fragment import Fragment

    data = Fragment(
        is_root_doc=True,
        main_header="some title",
        markup="md",
        text="test text",
        tags={"tag": ["test"]},
    )
    try:
        cache.read_cached_file(path)
    except LookupError:
        print('PASS reading non-existing cache file')
    else:
        raise Exception("cache should be empty")

    # setup and write to cache
    cache.write_file_to_cache(path, [data])
    print('PASS write file to cache')

    result = cache.read_cached_file(path)
    if not isinstance(result[0], Fragment):
        print(type(result), result)
        raise Exception("Reading cache doesn't work")
    print('PASS reading file from cache')

    # clean up test cache
    rmtree(cache.cache_path)
