#!/usr/bin/python3
"""
Functions to look through files and extract documentation fragments from comments.
Check the main Doc3n documenation for info.

$doc:rst Doc3n Gather fragments

All the spread out information, fragments_ (see `doc3n writing documentation`_), has to be
collected. This is the first part of two in a build script using Doc3n_. The second
part is exporting or `doc3n generate documents`_.

The split up makes it possibility to spread out documentation over multiple sites, see Cache_.


Find documentation fragments
----------------------------

Extract documentation fragments
-------------------------------

Cache
-----

$in:rst Gather codebase
The extraction of documentation fragments is done with the gather-library in doc3n. It can be
used separately from the exporter, to reduce dependencies, for running on multiple hosts f.ex.
on f.ex. multiple hosts.

The steps in the gather process, and where the code for each step, mainly is:

1. search files and folders; `<$file:gather/find.py>`_
2. extract fragments, (tagged with $in:...); `<$file:gather/fragment.py>`_
3. cache result to speed up recurrent runs; `<$file:gather/cache.py>`_

The cache can cause confusion while working with gathering. Check cache_ on how to step around that.
"""

import os
import logging
from typing import Iterator

"""
Setup to control logging from environment (also used in rest of Doc3n).

$doc:rst Doc3n Troubleshooting

Logging
=======

The levels of logging in different areas of the code can be controlled separately, for
example to debug file finding, parsing or exporting. This can be done in code or from
environment variables. Available loggers are listed below.

In code, the correct logger can be fetched and updated like this::

    logging.getLogger("doc3n.export").setLevel(logging.DEBUG)

From environment, set before your build script. Logger can be written with dots or
underscore, and available levels are debug, info, warning, error. Case insensitive::

    env doc3n.find=debug ./make_doc.py
    doc3n_export_page=WARN ./make_doc.py

"""
logging.basicConfig(format='%(asctime)s %(levelname)s %(name)s:  %(message)s')
# remove pypandoc warnings (in export)
logging.getLogger("pypandoc").setLevel(logging.ERROR)
# check env preferences for logging
LOG = logging.getLogger("doc3n")
LOG.setLevel(logging.INFO)
for key, val in os.environ.items():
    if key.startswith("doc3n"):
        logger_name = key.replace("_", ".")
        logging.getLogger(logger_name).setLevel(val.upper())

def logger_names() -> 'Iterator[str]':
    "Get a list of loggers"
    yield from sorted(LOG.manager.loggerDict)


# Make these available to init and export
# be careful changing these (keep some api complicance for build scripts)

from .gather import Gather as Gather
from .extractor import ExtractorComments as ExtractorComments
from .extractor import ExtractorFullText as ExtractorFullText
from .find import find_content as find_content
from .find import find_content_string as find_content_string
from .find import Filter as Filter
from .fragment import Fragment as Fragment
from .cache import get_cache as get_cache
from .cache import get_no_cache as get_no_cache
from .cache import Cache as Cache
