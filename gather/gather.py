#!/usr/bin/python3
from typing import List, Union, Optional
from pathlib import Path

from .extractor import ExtractorComments, ExtractorFullText
from .find import find_content, find_content_string, Filter
from .cache import get_cache, Cache
from .fragment import Fragment

"""
$in:rst Find documentation fragments
Fragments can be extracted from files, recursively from paths and directly
given as text. The extraction is cached, see cache_, and only updated files
are processed, so the process is relatively quick.

Caching context
---------------
Using different cache folders when collecting data, opens up for configuring each context
differently during exports. The ``Gather``-object, takes a cache, a path to the cache or
a string representing the subpath of the cache, working as a context.

"""

class Gather:
    """Used to collect documentation fragments from code and cache it.
    It is also possible to skip caching or read from cache instead.

    Instance is later used by export library.

    $in:rst Find documentation fragments
    A collector, called ``Gather``, is used to handle the fragments. It contains
    default filters and default cache, and has two functions to add either texts or
    files/folders. The documentation is generated with the help of this object, and
    it can read from an existing cache context (see `transfere cache`_).

    $in:rst File Filter
    When using gather, a filter with the default Filter settings is always used.
    """

    def __init__(self,
                cache: str|Cache,
                remove_old_cached_files = True,
                filter: Optional[Filter] = None) -> None:
        self.__filter = filter or Filter()
        self.__fragments: List[Fragment] = []
        self.cache = get_cache(cache, remove_old_cached_files)

    def read_from_cache(self) -> None:
        """Read fragments from cache instead of file
        (usefull when gathering and rendering is separated, f.ex.
         for distributed systems)."""
        self.__fragments.extend(self.cache.read_fragments())

    def add_path(
        self,
        root: Union[Path, str],
        path: Union[Path, str],
        extractor: Union[ExtractorComments, ExtractorFullText, None] = None,  # if a special one is needed to these files.
        source_prefix: Union[Path, str, None] = None,
        filter: Optional[Filter] = None,
        skip_cache: bool = False,
    ) -> None:
        """
        Look for fragments from files under path, See find.py and find_content
        """
        if not isinstance(root, Path):
            root = Path(root)
        if not isinstance(path, Path):
            path = Path(path)
        if source_prefix is not None and not isinstance(source_prefix, Path):
            source_prefix = Path(source_prefix)

        self.__fragments.extend(
            find_content(
                root,
                path,
                cache=self.cache,
                extractor=extractor,
                source_prefix=source_prefix,
                filter=filter or self.__filter,
                skip_cache=skip_cache,
            )
        )

    def add_text(self, text: str,
                 extractor: Union[ExtractorComments, ExtractorFullText, None] = None,
                 cache_id: str|None=None) -> None:
        """
        Look for fragments in a text string, See find.py and find_content_string
        """
        self.__fragments.extend(
                find_content_string(text, cache=self.cache, extractor=extractor, cache_id=cache_id))

    def get_fragments(self) -> List[Fragment]:
        return self.__fragments[:]
