#!/usr/bin/python3

import logging
from pathlib import Path
import magic
from typing import List, Tuple, Iterator, Union, Optional

from .fragment import extract_fragments, Fragment
from .extractor import (
    Extractor,
    ExtractorComments,
    ExtractorFullText,
    fullTextExtractor,
)
from .cache import Cache, CACHE_FOLDER_FILE

LOG_MIME = logging.getLogger("doc3n.find.mime")
LOG_FILE = logging.getLogger("doc3n.find.file")
LOG_READ = logging.getLogger("doc3n.find.read")
LOG_FILTER = logging.getLogger("doc3n.find.filter")

# Files that can be exluded directly
# and files that may have uncertain mime type
DEFAULT_EXCLUDE_GLOB = [
    # tmp files
    "*.pyc",
    "*.orig",
    "*.swp",
    "*.bak",
    "*.tmp",
    "*.~",
    # folders
    ".git",
    ".hg",
    "__pycache__",
    ".mypy_cache",
    ".vscode",
    ".ignore",
    "cdk.out",
]


class Filter:
    r"""
    $in:rst Find documentation fragments
    File Filter
    -----------
    When finding files, a file filter is used to skip files and folders that
    are unneeded. Each filter checks three different things.

    If the file is hidden and hidden files (dot-files) should be excluded.

    If the file matches any of the patterns to exclude (in glob format).

    If the file matches any of the default patterns (also in glob format).
    This can be overwritten if needed. Defined in
    this file (`file.py<$file:doc3n/gather/find.py>`_).

    To create a filter::

        # all settings are optional
        Filter(
            # skip matching files and folders - in addition to default list
            exclude_globs=["*.doc", "*.swx", "cdk.out"],
            # to not skip hidden files - default is to exclude them
            exclude_dotfiles=False,
            # to overwrite default exclude list
            default_exclude_glob = [...])

    """

    def __init__(
        self,
        exclude_globs: Optional[List[str]] = None,
        exclude_dotfiles: bool = True,
        default_exclude_globs: List[str] = DEFAULT_EXCLUDE_GLOB,
    ):
        # exclude glob custom filer
        self.__exclude_globs = exclude_globs
        self.__default_exclude_globs = default_exclude_globs
        # hidden, dotfiles
        self.__exclude_dotfiles = exclude_dotfiles

    def skip(self, rel_path: Path) -> bool:
        "Check if path should be excluded"
        if self.__exclude_dotfiles:
            if rel_path.name.startswith("."):
                LOG_FILTER.debug('Exclude hidden folder %s', rel_path)
                return True
        for glob in self.__exclude_globs or []:
            if rel_path.match(glob):
                LOG_FILTER.debug('exclude file %s due to exclude globs', rel_path)
                return True
        for glob in self.__default_exclude_globs:
            if rel_path.match(glob):
                LOG_FILTER.debug('exclude file %s due to default exclude globs', rel_path)
                return True
        return False


def mime_from_path(full_file_path: Path) -> "Tuple[str, str]":
    """
    Get mime type from path as ("type", "subtype").

    Different versions of python libmagic bindings complicates this...

    $in:rst Setup Doc3n

    libmagic requirement
    --------------------
    Building require python bindings for libmagic. There are some different
    versions with similar name, some working and some not. Debian seems to ship
    a good version as usual. RedHat doesn't work well in older environments at least.
    """
    mime: str = (magic.Magic(mime=True, mime_encoding=True)
                .from_file(str(full_file_path))
                .replace("/", " "))
    # e.g. application xml; charset=iso-8859-1
    if mime == "application octet-stream":
        # skip binaries and empty files
        file_mime, charset = "", ""
        LOG_MIME.debug("%s => skip application/octet-stream", full_file_path)
    else:
        try:
            file_mime, charset = mime.split("; charset=")
            # ADHOC fixes. May need to be changed to something more robust or flexible
            if charset == 'us-ascii':
                charset = 'utf-8'  # safer default value if mime check fails
            elif charset == 'unknown-8bit':
                charset = 'iso8859'
            LOG_MIME.debug("%s => %s %s", full_file_path, file_mime, charset)
        except ValueError:
            LOG_MIME.error(
                'Cannot parse mime string "%s" for file %s', mime, full_file_path
            )
            file_mime, charset = "", ""
    return file_mime, charset


def find_content_string(
    content: str,  # look through this text string
    cache: Cache,
    mime: str = "text",  # mime for the file
    extractor: Union[ExtractorComments, ExtractorFullText, None] = None,  # how to gather fragments
    cache_id: str|None = None
) -> Iterator[Fragment]:
    """
    $in:rst Find documentation fragments
    From text
    ---------
    It is possible to add text directly that isn't in some file. This is
    useful for generated content (like in the logging_ section in this
    document)::

        gather = Gather("cache_name")
        gather.add_text(text [, cache_name])
    """
    if not extractor:
        extractor = Extractor.get(mime) or fullTextExtractor
    fragments = list(extract_fragments(
        extractor, content=content, mime=mime, cache=cache))
    if cache_id:
        cache.write_text_to_cache(cache_id, fragments)
    yield from fragments


def read_fragments_from_file(
    root: Path,
    rel_path: Path,  # assume file exists
    cache: Cache,
    extractor: Union[ExtractorComments, ExtractorFullText, None] = None,
    source_prefix: Union[Path, None] = None,
) -> Iterator[Fragment]:
    full_path = root.joinpath(rel_path)
    file_mime, encoding = mime_from_path(full_path)
    if full_path.stat().st_size < 10:
        # skipp small files
        return
    if not extractor:
        extractor = Extractor.get(file_mime, full_path)
    if extractor:
        LOG_READ.debug(
            "%s (%s %s) %s", full_path, file_mime, encoding, extractor
        )  # show ALL files
        try:
            with open(full_path, "r", encoding=encoding) as handle:
                content = handle.read()
                yield from extract_fragments(
                    extractor=extractor,
                    content=content,
                    cache=cache,
                    file_path=rel_path,
                    mime=file_mime,
                    source_prefix=source_prefix,
                )
        except LookupError as err:
            LOG_FILE.warning("Cannot load file %s; %s", full_path, err)
    else:
        LOG_MIME.debug("No extractor found for mime type %s", file_mime)


def find_content_file(
    root: Path,
    rel_path: Path,
    cache: Cache,
    extractor: Union[ExtractorComments, ExtractorFullText, None] = None,
    source_prefix: Union[Path, None] = None,
    skip_cache: bool = False
) -> Iterator[Fragment]:
    full_path = root.joinpath(rel_path)
    if not full_path.exists():
        LOG_FILE.warning("File path does not exist: %s", full_path)
    elif not full_path.is_file():
        LOG_FILE.warning("Path must point at a file: %s", full_path)
    else:
        try:
            try:
                yield from cache.read_cached_file(full_path)
            except LookupError:
                fragments = list(
                    read_fragments_from_file(
                        root, rel_path, cache, extractor, source_prefix
                    )
                )
                if not skip_cache:
                    cache.write_file_to_cache(full_path, fragments)
                yield from fragments
        except PermissionError as err:
            LOG_FILE.error(err)


def find_content(
    project_root: Path,
    rel_path: Path,
    filter: Filter,
    cache: Cache,
    extractor: Union[ExtractorComments, ExtractorFullText, None] = None,
    source_prefix: Union[Path, None] = None,
    skip_cache: bool = False
) -> Iterator[Fragment]:
    """
    $in:rst Find documentation fragments
    From a path (or file)
    ---------------------
    A path can be searched for files to be extracted.
    A filter is used to exclude unwanted files and folders and make
    the process quicker (f.ex. temp files and project management folders).

    Folders are searched recursively, but symlinks are not followed.

    A suitable way to extract the content is then selected based on mime
    type and file ending. See `doc3n writing documentation`_ for supported formats
    and how to add new ones.

    Paths are specified by a root path and relative path. The root path is
    used to build an absolute path to each file, but is later discarded so
    the project can be used independent on location and environment.

    If a source_prefix is added, the output path for the file is the
    source_prefix and relative path combined.

    An example::

        gather = Gather("example_cache_name")
        gather.add_path('/home/my/Documents/Doc3n/', 'example/makedoc.py')

    To limit the files to include, check `file filter`_.

    It is also possible to specify a specific extractor, f.ex. if there
    are some obscure files in a specific folder, without ending or known
    mime type.
    """
    full_path = project_root.joinpath(rel_path)

    if not full_path.exists():
        LOG_FILE.warning("Path does not exist: %s", full_path)

    elif full_path.is_file():
        # Ignore filter if find content is called with a file path
        yield from find_content_file(
            root=project_root,
            rel_path=rel_path,
            cache=cache,
            extractor=extractor,
            source_prefix=source_prefix,
            skip_cache=skip_cache)

    elif filter.skip(rel_path):
        LOG_FILE.debug("path filtered out %s", full_path)
        return

    elif full_path.joinpath(CACHE_FOLDER_FILE).exists() or full_path.parent.joinpath(CACHE_FOLDER_FILE).exists():
        LOG_FILE.debug("cache path skipped %s", full_path)
        return

    elif full_path.is_dir() or full_path.is_symlink():
        # symlinks are only allowed when pointed at directly
        LOG_FILE.debug("Check all files recursively under: %s", full_path)
        for full_sub_path in sorted(full_path.iterdir()):
            rel_sub_path = full_sub_path.relative_to(project_root)
            if full_sub_path.is_symlink():
                LOG_FILE.debug("Symlinks are not followed %s", full_sub_path)
            elif filter.skip(rel_sub_path):
                LOG_FILE.debug("path file filtered out %s", full_sub_path)
                continue
            else:
                if filter.skip(rel_sub_path):
                    LOG_FILE.debug("path filtered out %s", full_sub_path)
                    return
                yield from find_content(
                    project_root,
                    rel_sub_path,
                    filter=filter,
                    cache=cache,
                    extractor=extractor,
                    source_prefix=source_prefix,
                    skip_cache=skip_cache)

    else:
        LOG_FILE.warning('Skipping entry (%s) (not file, symlink or folder)', full_path)


def test() -> None:
    logging.basicConfig(level=logging.DEBUG)
    import sys
    from .cache import get_no_cache

    for name in sys.argv[1:]:
        print("Find content in", name)
        for fragment in find_content_file(Path(""), Path(name), cache=get_no_cache()):
            print(" ", fragment)

    # for block in find_content('../', '__init__.py', None):
    #    print block.content

    # for block in find_content('.', ''):
    #    logging.info(block)
    # for s in find_content_string(open('lib/export.py').read(), 'text python'):
    #    print(s.content)


if __name__ == "__main__":
    test()
