#!/usr/bin/python3
"""
$in:rst Doc3n Writing Documentation

Source code formats
===================

Different file formats have different ways of defining comments and
detection is a bit fuzzy. Doc3n_ aims to support the most common cases
for the most standard formats, like c-style, xml-comments and python
triple quotes.

Supported formats
-----------------

Indentation
===========


$in:rst Extract documentation fragments

When searching for documentation fragments_ Doc3n first finds potential comment
blocks. Then examine each of them for any fragment. Looking for fragments is done
in a general way, but how to find the comment blocks is specific to each file type.
"""

import re
import logging
from pathlib import Path
from typing import Dict, Tuple, Iterator, List, Optional, Union


LOG_COMMENT = logging.getLogger("doc3n.find.comment")
LOG_EXTRACTOR = logging.getLogger("doc3n.find.extractor")


class Extractor:
    r"""
    $in:rst Extract documentation fragments
    An extractor, a rudimentary code analyzer, is needed for each supported file format.
    Many formats use the same type of syntax, so it can be reused though. Check
    `supported formats`_ to see what is already built in, and how to add new file formats.

    Note that the extractors are simple, so advanced usage of comments can cause some fragments
    to be missed, or to include too much.

    $in:rst Supported formats

    Add new format
    ==============

    Each format is defined by a regexp to find start and another to find end of comment.
    Some are predefined, but it is also possible to add new formats with the ``Extractor``.

    It takes a mime type and zero or more regexp pairs.

    The mime type is space separated, e.g. "text python". If you want to match on the group, add
    a mime type with only the group, e.g. "text" (both are already defined).

    Each regexp pair describe a regexp for the starting tag of a comment,
    and one for the end tag (when a start tag is found). Python standard re.search is used.

    Zero pairs means everything in the file is a comment (used for text formats).

    From these, an "Extractor" is created. The created object can be used explicit when adding
    files, but in most cases it is enough to create it and it is automatically used when finding
    matching files.

    An example::

        Extractor(
            ["text c"],
            ["c"],
            (r'"', r'"'),
            (r"/*", r"*/"),
            (r'\n\s*#', r'\n\s*[^\s]|$')
        )
    """
    from_mime: Dict[str, Union["ExtractorComments", "ExtractorFullText"]] = {}
    from_ending: Dict[str, Union["ExtractorComments", "ExtractorFullText"]] = {}

    def __init__(
        self, mimetypes: List[str], endings: List[str], globaly_known: bool = True
    ):
        self.mimetypes = mimetypes
        self.endings = endings
        if isinstance(self, (ExtractorComments, ExtractorFullText)):
            if globaly_known:
                for mimetype in mimetypes:
                    self.from_mime[mimetype] = self
                for ending in endings:
                    self.from_ending[ending] = self
        else:
            raise Exception("Use ExtractorComments or ExtractorFullText instead")

    @classmethod
    def get(
        cls, mimetype: str, filename: Optional[Path] = None
    ) -> Union["ExtractorComments", "ExtractorFullText", None]:
        """Return matcher regexps for a mime type"""
        extractor = cls.from_mime.get(mimetype)
        if extractor:
            LOG_EXTRACTOR.debug(
                "Matched %s based on mime '%s' with %s", filename, mimetype, extractor)
        if not extractor and filename:
            file_ending = "." + str(filename).split(".")[-1]
            extractor = cls.from_ending.get(file_ending)
            if extractor:
                LOG_EXTRACTOR.debug(
                    "Matched %s (mime %s) based on ending with %s", filename, mimetype, extractor)
        if not extractor:
            main_mime = mimetype.split(" ")[0]
            extractor = cls.from_mime.get(main_mime)
            if extractor:
                LOG_EXTRACTOR.debug(
                    "Matched %s based on mime '%s' with %s", filename, main_mime, extractor)
        if not extractor:
            LOG_EXTRACTOR.debug(
                "No extractor found matching mime '%s' or file '%s'", mimetype, filename)
        return extractor


class ExtractorComments(Extractor):
    def __init__(
        self,
        mimetypes: List[str],
        endings: List[str],
        *quotes: Tuple[str, ...],
        globaly_known: bool = True
    ):
        Extractor.__init__(self, mimetypes, endings, globaly_known)
        # end pattern for each start pattern group
        self.ends: List[re.Pattern[str]] = []
        # indentation to remove, f.ex. # or \s
        self.indentation: List[re.Pattern[str]] = []
        pattern_starts: List[str] = []
        for quote in quotes:
            pattern_starts.append("(%s)" % quote[0])
            self.ends.append(re.compile(quote[1]))
            self.indentation.append(re.compile(quote[2] if len(quote) >= 3 else r"\s*"))
        # start patterns in one regexp
        self.start = re.compile("|".join(pattern_starts))

    def __str__(self) -> str:
        return "Extractor {mimes} {endings} {start} {end}".format(
            mimes=self.mimetypes,
            endings=self.endings,
            start=self.start.pattern if self.start else "",
            end=" ".join([e.pattern for e in self.ends or []]),
        )


class ExtractorFullText(Extractor):
    def __init__(
        self, mimetypes: List[str], endings: List[str], globaly_known: bool = True
    ):
        Extractor.__init__(self, mimetypes, endings, globaly_known)

    def __str__(self) -> str:
        return "Extractor full text"


"""
$in:rst Supported formats

The support for the formats isn't perfect but work in most cases. The following
formats have some kind of support:

* python
* c
* (rave)
* xml
* x-shellscript (`shell scripts`)
* pure text

shell scripts
'''''''''''''
For shell scripts we need to define a semantic and use it.
Here are three suggestions, currently understand by doc3n::

    : <<END_DOC
    multi line comment
    END_DOC

    : '''
    multi line comment
    '''

    # multiple single line
    # comments

    # empty lines allowed
    #

"""
TRIPLE_QUOTE = ("'''", "'''")
TRIPLE_DOUBLE_QUOTE = ('"""', '"""')
C_STYLE = (r"/\*", r"\*/")
XML_STYLE = (r"<!\-\-", r"\-\-")
HASHED_LINES = (r"\n\s*#", r"\n\s*([^#\s]|\n|$)", r"\s*#\s*")
DOUBLEDASH = (r"\-\-", r"\n^\-\-", " \t--")
SHELLSCRIPT_TRIPLE_QUOTE = (r"\n: '''", r"\n'''")
SHELLSCRIPT_END_DOC = (r"\n: <<END_DOC", r"\nEND_DOC")

# mime type matching mime_from_path
ExtractorComments(
    ["text python", "text x-python"],
    [".py"],
    TRIPLE_QUOTE,
    TRIPLE_DOUBLE_QUOTE,
    HASHED_LINES,
)
ExtractorComments(
    ["text c", "text c++", "application javascript", "text rave"],
    [".c", ".js"],
    C_STYLE,
)
ExtractorComments(["text xml", "application xml"], [".xml"], XML_STYLE)
ExtractorComments(
    ["text x-shellscript"],
    [".sh"],
    SHELLSCRIPT_TRIPLE_QUOTE,
    SHELLSCRIPT_END_DOC,
    HASHED_LINES,
)
ExtractorComments(["text yaml", "application yaml"], [".yml", ".yaml"], HASHED_LINES)

fullTextExtractor = ExtractorFullText(["text"], [])


def extract_comments(
    extractor: Union[ExtractorFullText, ExtractorComments], mime: str, content: str
) -> Iterator[Tuple[str, int]]:
    """yield comments from a string, mime type is mime split by space"""
    line_nr = 0
    if isinstance(extractor, ExtractorFullText):
        if content:
            yield content, 0
        return

    remaining_start = 0
    remaining_content = content
    while True:
        match_comment_start = extractor.start.search(remaining_content)
        if not match_comment_start:
            return

        comment_start: int = match_comment_start.end()
        for match_group_nr, group in enumerate(match_comment_start.groups()):
            if group is not None:
                break
        else:
            raise Exception("No group found in regexp, extractor is faulty - should not be possible!")
        line_nr += remaining_content.count("\n", 0, comment_start)

        remaining_content = remaining_content[comment_start:]

        match_comment_end = extractor.ends[match_group_nr].search(remaining_content)

        if match_comment_end:
            comment_end, remaining_start = match_comment_end.span()
        else:
            comment_end = None

        text = remove_indentation(
            remaining_content[:comment_end], extractor.indentation[match_group_nr])

        if not match_comment_end:
            # can cause a lot of errors for f.ex. regexps
            LOG_COMMENT.debug(
                "Comment end not found for mime %s, extractor %s, content %s",
                mime,
                extractor.ends[match_group_nr].pattern,
                remaining_content,)
            yield text, line_nr
            return

        yield text, line_nr
        line_nr += remaining_content.count("\n", 0, remaining_start - 1)
        remaining_content = remaining_content[remaining_start - 1 :]


def remove_indentation(content: str, prefix: "re.Pattern[str]") -> str:
    """
    :param content: string that may have indentation.
    :return: content with removed indentation

    $in:rst Indentation

    It is assumed all content in a documentation block has the same base indentation.
    The smallest common indentation in each document fragment is removed from each line.

    One exception is the first line which is ignored, to allow definitions directly
    after comment tag.
    """
    lines = content.split("\n")
    # figure out indentation
    indentation = None
    for line in lines[1:]:
        match = prefix.match(line)
        if match and len(line) > match.end():
            #  else assume empty line, skip it
            new_ind = match.end()
            if indentation is None or new_ind < indentation:
                indentation = new_ind
    if indentation is None:
        return ""

    # remove indentation
    fixed_lines = [lines[0]]
    for line in lines[1:]:
        # removed = line[:indentation]
        fixed_lines.append(line[indentation:])
    return "\n".join(fixed_lines)


def test() -> None:
    """Some basic tests of this module"""
    mime = "text python"
    extractor = Extractor.get(mime)
    if not extractor:
        print("failed, extractor not found")
    else:
        print(extractor)
        for c in extract_comments(
            extractor,
            mime,
            """
        #!/usr/bin/python3

        #   \u0024doc:rst TEST
        #   content main
        #
        #   head 1
        #   ######
        #   content 1
        #
        #   \u0024in:rst head 1
        #   $tag_1:

        '''
        \u0024in:rst head 1
        content main

        head 1 2
        ########
        content 1 2
        '''

        #   Skip this line
        """,
        ):
            print("Content:", c)


if __name__ == "__main__":
    test()
