#!/usr/bin/python3
"""
Main module and main entry point for Doc3n

Doc3n is split up in two parts, gather and export, in corresponding folders.
First all documentation pieces, snippets, are gathered and cached. Then the
pieces are parsed, stitched together and exported.

This document is generated by "/examples> python makedoc_doc1.py", look in the
documentation under examples/html_page or examples/html_pages for a better
experience.

First document definion and fragment starts right below:

$doc:rst Doc3n

High level code documentation embedded in code, trying to fill the gap between
pure api and business docs and is meant to be used for code usage,
configurations, guides, external api's combined.

Making the documentation more usefull and easier to keep up to date in wide
projects. In many cases the overview of a project doesn't map well to file
structure, and processess and tools may be more usefull than direct api's in
many cases.

By writing documentation close to the code it applies to, it is easier for
developers to keep it up to date and to keep it relevant for the specific area,
may it be configuration or usage.

Doc3n then generates aggregated documents from the different files and can
generate different type of documentation for interfaces, external references,
internal guides etc.

For internal overview of a project Doc3n can include reference to the file that
contains a piece of the documentation making it easier to find special areas.

======
Basics
======

Documentation is written in comments or pure text files. Each piece of
information starts with a tag telling where it belongs and what markup it uses.

Worth noting is that the structure of the documentation usually doesn't reflect
the file structure and the information for a specific area may be spread in
many files and folders.

To generate the documentation one or more small scripts has to be written to
configure how to collect documentation fragments, stitch them and export them to
files in the format of choise.

Doc3n uses pandoc_ to convert between different formats. It is thanks to this
powerful tool that Doc3n is so flexible in handling different formats.

An initial example can be found in `<$file:examples/makedoc_doc3n.py>`_ which
puts together the readme-files for Doc3n_.


============
More details
============

How should documentation be written for Doc3n: `Doc3n Writing Documentation`_.

How to setup Doc3n to gather information from documentation snippets:
`Doc3n Gather fragments`_.

How to setup Doc3n to stitch the information and export documents:
`Doc3n Generate documents`_.

When gather or export fail: `Doc3n troubleshooting`_.

If you want to tweak or help evolve Doc3n: `Doc3n codebase`_.

Known things that probably should be fixed with Doc3n: `Doc3n Issues`_.

========
Glossary
========

pandoc
------
A powerful tool to convert between different markup formats,
`<http://pandoc.org>`_.

comment
-------
Code comment, varies between different text file languages.

fragment
--------
A documentation piece, unprocessed.

snippet
-------
See fragment_.

context
-------
It is possible to collect information from multiple contexts, for example from
multiple servers. Each context can have it's own settings when exporting.


$doc:rst Doc3n Codebase

`Doc3n`_ is split in two main parts. First part is used to find files and extract
the fragments.  (placed under `gather/<$file:gather/>`_). Second part to make actual
pages of it and to export it to different formats.

This split makes it possible to gather fragments from a lot of places (plan is to
make the gather tool to have less requirements). And generate all needed documents
in one place, making it possible to cross reference accross different environments.

Gather codebase
===============

Export codebase
===============
"""

import sys
import timeit
import logging
from pathlib import Path
import shutil
from typing import List


# make these easily available
# if anything changes here, build scripts may stop working
from .gather import (
    Gather as Gather,
    get_cache as get_cache,
    get_no_cache as get_no_cache,
    Filter as Filter,
    logger_names as logger_names,)
from .export import (
    to_page as to_page,
    to_pages as to_pages,
    stitch as stitch)

# only used by scripts generating documentation
LOG_BUILD = logging.getLogger("doc3n.build")


def path_from_main_script() -> Path:
    return Path(sys.argv[0]).parent



class TemporaryFolder:
    """Write to a Create a temporary folder to write content to, and clear it and 
    useage: with TempoaryFolder(someFolder, tmp_suffix=tmp) as tmp_folder:
        write to tmp_folder
    <tmp_suffix> defaults to '.tmp'
    When leaving "with", replace folder with the tmp-folder,
    """
    def __init__(self, folder: Path, tmp_suffix='.tmp'):
        self.folder = folder
        self.tmp_folder = self.folder.with_suffix(tmp_suffix)

    def __enter__(self) -> 'TemporaryFolder':
        if self.tmp_folder.exists():
            shutil.rmtree(self.tmp_folder)
        self.tmp_folder.mkdir()
        return self.tmp_folder

    def __exit__(self, exception_type, exception_value, exception_traceback):
        if exception_type:
            return
        shutil.rmtree(self.folder)
        self.tmp_folder.rename(self.folder)
 
def clear_folder(path: Path, extensions: List[str]):
    if not path.exists():
        path.mkdir(exist_ok=True)
    else:
        for file in path.glob('*.*'):
            if file.suffix in extensions:
                file.unlink()



class TimeCode:
    """Helper to time code
    Good for checking how long time it takes to find content
    """

    def __init__(self) -> None:
        self.start = timeit.default_timer()
        self.last_step = self.start

    def step(self) -> float:
        now = timeit.default_timer()
        diff = now - self.last_step
        self.last_step = now
        return diff

    def total(self) -> float:
        return timeit.default_timer() - self.start
