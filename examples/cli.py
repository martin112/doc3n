#!/usr/bin/python3
"""
Script to populate doc3n cache from terminal, and generate documentaiton for it.

Mainly for debugging documenation. A generator script is more maintainable and
more flexible.

Example:
    path/to/files/ > python3  path/to/doc3n/examples/cli.py  include_file include_folder -f html 
    # generate path/to/files/doc3n_cli.html
    # cache in path/to/files/doc3n_cli_cache

"""
from argparse import ArgumentParser
import logging
from pathlib import Path

from doc3n import Gather, stitch, to_page
from doc3n.gather import Cache
from doc3n.export import Exporter

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


def main():
    parser = ArgumentParser(description="Generate documentation from terminal")
    parser.add_argument('paths', nargs='*', help="file paths to read into cache")
    parser.add_argument('-f', dest='format', help='build for format')
    parser.add_argument('-c', dest='clear', help='clear cache', action='store_true')
    args = parser.parse_args()

    # setup cache
    cache_path = Path('.').joinpath('doc3n_cli_cache').joinpath('main')
    cache = Cache(cache_path, remove_old=False)
    gather = Gather(cache)

    # cache files
    for path in [Path(p) for p in args.paths]:
        if path.is_dir() or path.is_file():
            gather.add_path('', path)
        else:
            log.info('Skipping %s', path)                             


    # generate output
    # for now single document to stdout
    if args.format:
        # also read previously cached files
        from_cache = Gather(cache)
        from_cache.read_from_cache()
        from_cache.add_text('$doc:rst doc3n_cli')
        documents = stitch(from_cache)
        include_pages = [d.title for d in documents.documents]
        if not include_pages and not documents.orphans:
            log.warning('No pages found')
            return 1
        else:
            page = to_page(documents, 'doc3n_cli')
            exporter = Exporter(page, format=args.format, include_pages_ordered=include_pages)
            output_file = Path('doc3n_cli.{}'.format(args.format))
            exporter.export(output_file)
            log.info('exported to %s', output_file)
    return 0

if __name__ == '__main__':
    exit(main())
