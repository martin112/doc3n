#!/usr/bin/python3
"""

Rewrite document so this is in examples, maybe even seprate documents

Gather examples

Export examples


$doc:rst Doc3n Export documents

When you have added documentation to your project you need to generate documents
from it that are presentable.

For this a build script is used. It controls what files and folders to include,
and how to combine and export the fragments into documents of wanted formats.

The build script for the Doc3n documentation, `<$file:makedoc_doc3n.py>`_ is a good
start example.

Setup Doc3n
===========

Requirements for running Doc3n and how to set it up.

:Python3: Doc3n runs on `Python3<http://python.org>`_.

:Pandoc_:
    All markup conversion is done by Pandoc_ and the
    `pypandoc module<https://pypi.org/project/pypandoc/>`_, this is needed for generating
    and exporting documents, not for gathering them. To install ::

        pip install pypandoc_binary

:Magic:
    Frontend to libmagic is used for detecting file types. Install with your
    local package manager or for example pip::

        sudo pip install python-magic

If all those run, create your build file as described below, and hit it.

"""
import logging
logging.basicConfig(level=logging.INFO)

"""
To read out fragments First you have to read documentation fragments out from
files or text strings. This is by default cached to speed up extraction.

An instance of Gather is used to handle default filters, cache settings etc.

See documentation under `Gather fragments`_.
"""
from pathlib import Path
from doc3n import Gather, logger_names

gatherer = Gather('Doc3n')

PROJECT_ROOT = Path('../')

gatherer.add_path(PROJECT_ROOT, '__init__.py')
gatherer.add_path(PROJECT_ROOT, 'gather')
gatherer.add_path(PROJECT_ROOT, 'export')
gatherer.add_path(PROJECT_ROOT, 'examples/makedoc_doc3n.py')
gatherer.add_path(PROJECT_ROOT, 'issues')

logger_names_str = '\n'.join([f'* {name}' for name in logger_names()])

# use \u0024 to prevent strings to show up in documentation
gatherer.add_text(f"""
\u0024in:rst Logging
All loggers
-----------
{logger_names_str}

""")

"""
$in:rst Doc3n Generate documents

After collecting the documentation fragments, this step stitches together
them into one or many pages::

    page = doc3n.page()
    pages = doc3n.pages()

Lost documentation fragments
----------------------------

If a documentation fragment is pointing at an unknown header,
it will be put in a special section. This will cause warnings
to the terminal when generating documents.

Default setting is to include them in a page/chapter called
`lost sections`_. This can be modified when creating the page/pages::

    page = doc3n.page(lost_sections_title=None)
    pages = doc3n.page(lost_sections_title="Bortkommna texter")

$in:rst intentionally lost section
This section (``$in:rst intentionally lost section``) is an example
of a section that is defined to be under a header that does not exist.
Should be the only paragraph in lost sections in this documentation.
"""

from doc3n import to_page, to_pages, stitch
documents = stitch(gatherer)
pages = to_pages(documents, lost_sections_title="Lost sections")
page = to_page(documents, 'Doc3n')

"""
$in:rst Doc3n Generate documents

Post processing
---------------

Quite often you'd like to change something before exporting. For example
adding headers or footers.

Page and pages has some helper methods for this. And under doc3n.pandoc there
are some helpers to modify the internal intermediate format.

For example, adding a header to each page::

    pages.mod_pages(lambda p: p.blocks.insert(0, doc3n.pandoc.paragraph("version 0.1"))

"""

from datetime import datetime
from doc3n.export import pandoc

header_text = "Generated %s" % datetime.now().isoformat(' ')[:16]
pages.insert_content(
    pandoc.create_paragraph(pandoc.create_text(header_text)), 0)

"""
$in:rst Doc3n Generate documents

Save Documents
==============

When all fragments have been stitched together to one or many pages,
they can be exported to any format supported by pandoc.

When exporting, you specify format and file or folder to export to.

Each format has a bit different behavious. Doc3n tries to do sane
default choises, and it has tweaks for a few formats (so far).

To export:: python

    page.export(format='rst', output_file='readme.rst')
    pages.export(format='html5', output_folder='html_pages',
        settings={
            'source_prefix': '../',
            '--css': '../resources/style1.css',
        })

Exporters
---------

Export settings
---------------

format
''''''
What to convert the document to. In theory all Pandoc supported formats
are supported, but some need tweaks. Some examples::

:rst: Restructured Text
:md: Markdown
:html5: Html (there is a html export version too)
:pdf: Portable document format

output_file
'''''''''''
For single page documents, the file to write to.

output_files
''''''''''''
For multiple pages, files are written to this folder.

"""

from doc3n.export.exporter import Exporter

# html pages

output_pages_html = Path('html_pages/')
output_pages_html.mkdir(exist_ok=True)
exporter_html = Exporter(pages, 'html5',
    include_source = True,
    file_prefix = '../',
    source_prefix = '../../',)
exporter_html.formatter.output_css = '../resources/style1.css'
exporter_html.export(output_pages_html)

# html pages for gitlab

exporter_gitlab = Exporter(pages, 'html5',
    file_prefix='https://gitlab.com/martin112/doc3n/-/blob/master/',
    source_prefix='https://gitlab.com/martin112/doc3n/-/blob/master/',
    include_source=True,)
exporter_gitlab.formatter.source_link_pattern_with_line_nr = '{url}#L{line_nr}'
exporter_gitlab.formatter.output_css = 'style1.css'
exporter_gitlab.export(Path('gitlab'))

# single html page

output_page_html = Path('html_page/doc3n.html')
output_page_html.parent.mkdir(exist_ok=True)
exporter_html1 = Exporter(page, 'html5',
    append_referred_documents = True,
    include_pages_ordered = ["Doc3n writing documentation", "doc3n gather fragments", "doc3n generate documents"],
    file_prefix = '../',
    source_prefix = '../../',)
exporter_html1.formatter.output_css = '../resources/style1.css'
exporter_html1.export(output_page_html)

# single pdf file

exporter_pdf = Exporter(page, 'pdf',
         append_referred_documents=True,
         include_pages_ordered=["doc3n writing documentation", "doc3n gather fragments", "doc3n generate documents"],
         include_source=False,
         file_prefix='../',
         source_prefix='../../')
exporter_pdf.formatter.output_css = '../resources/style1.css'
exporter_pdf.export(Path('pdf/doc3n.pdf'))


# writing to a rdf-format

from doc3n.export.to_data import DataExporter, URL, Namespace, Prefix

mirdf_exporter = DataExporter(
    documents,
    collection_title='Doc3n project',
    prefix = Prefix('doc3n'),
    namespace = Namespace(URL('https://gitlab.com/martin112/doc3n/')),
    markup_language='html5')
# mirdf_exporter.formatter...
# mirdf_exporter.source_prefix = 'https://gitlab.com/martin112/doc3n/-/blob/master/',
mirdf_exporter.export(Path('mirdf/doc3n.mirdf'))
