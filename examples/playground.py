#!/usr/bin/python3
"""
Tool to try a text snippet out on the command line.

    playground.py {to format} {mime | filepath}

Content from stdin
------------------
to format is a pandoc supported writer
and mime is a mime type supported by doc3n CommentExtractor:

    
Example: 

    playground.py html5 text python
    some = "Python"
    '''
    &#36;doc:rst a comment
    Restructuredtext
    ----------------
    example
    '''

    playground.py odt application javascript < some/file.js

Content from file
-----------------
to format is a pandoc supported writer
and file is a relative or absolute file path to an
existing file. In this case stdin isn't used.

Example:

    playground.py plain ../lib/export_lib.py

"""

import sys
import os
import logging
from pathlib import Path
logging.basicConfig(level=logging.INFO)
log = logging.getLogger('playground')

from doc3n import Gather, stitch, to_page
from doc3n.gather import set_no_cache
from doc3n.export import Exporter

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Iterable, NoReturn

# For pandoc output
PLAYGROUND_TMP_FILE = Path('/tmp/doc3n.playground')

# skip caching
set_no_cache()

# Handle arguments
def help() -> 'NoReturn':
    print("""
    playground.py {to format} ...

        To read from stdin:
            playground.py {to format} {mime major} {mime minor}

            Example: playground.py html application python < examples/makedoc_doc3n.py
        
        To read from file:
            playground.py {to format} {file path}
        
            Example: playground.py md __init__.py
    """)
    sys.exit(3)


def extract_file(file_path: str) -> Gather:
    if not os.path.exists(file_path):
        log.error('File does not exist; %s', file_path)
        sys.exit(1)
    gather = Gather("playground")
    gather.add_path('', file_path)
    return gather


def extract_stdin(mime: str) -> Gather:
    content = sys.stdin.read()
    gather = Gather("playground")
    log.debug(content)
    gather.add_text(content)
    return gather

def out(to_format: str, gather: Gather) -> None:
    documents = stitch(gather)
    orphans = documents.make_orphans_document('separate sections')
    if orphans:
        documents.documents.append(orphans)
    headers = [d.header for d in documents]
    if headers:
        page = to_page(documents, headers[0])
        Exporter(page, to_format,
                 include_pages_ordered=headers[1:],
                 append_referred_documents=True,
                 include_source=True).export(PLAYGROUND_TMP_FILE)
        if PLAYGROUND_TMP_FILE.exists():
            print(open(PLAYGROUND_TMP_FILE).read())
            PLAYGROUND_TMP_FILE.unlink()
        else:
            log.warn('Empty output file')
    else:
        log.warn('No sections found')
        sys.exit(2)

if len(sys.argv) == 3:
    to_format = sys.argv[1]
    file_path = sys.argv[2]
    out(to_format, extract_file(file_path))
elif len(sys.argv) == 4:
    to_format = sys.argv[1]
    mime_major = sys.argv[2]
    mime_minor = sys.argv[3]
    out(to_format, extract_stdin('%s %s' % (mime_major, mime_minor)))
else:
    help()
