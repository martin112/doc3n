# Doc
Making documenation more usefull and easier to maintain.

Doc3n is a tool to generate documentation from files, mainly for software
projects. It's aim is to egenerate documenation that descibes purpose,
usage and overview rather than detailed code documentation.

Compared to other tools, like sphinx and doxygen, Doc3n isn't made specifically
to descibe low level apis.

Compared to separate documents, Doc3n keeps documentation where it belongs,
close to relevant definitions.

Doc3n also allows usage of most different markup languages, and it allows
mixing of them.

## Intention

Describing intention and usage of software is very important. But once
written, it often get neglected and diverge from the actual software.

Therefore the idea is to write those fragments of information as close to
the applicable code as possible. This makes the documentation a part of the
code base. It also makes it easier to know where to write it and lessens
the risk of missing updating it, because it is on the same screen as the
relevant code.

Compared to other tools the layout of the generated documentation is based
on the content not on what files it is placed in. The file structure isn't
in most cases relevant to the overview and related information is often
spread out in different files of different formats.

The generated documentation is suitable to use as an overview of the code
base for developers, by linking back to the actual source files where each
specific definition was created.

## Features

* Build readable documentation for people who aren't experts in the code base.
* Read out documentation fragments from multiple text based formats.
* Build on Pandoc - it supports writing in most markup languages, mixed.
* Build on Pandoc - it can generate documents in many different formats.
                    Some examples: dokuwiki, html, doc.

## Example and documentation

Indepth documentation can be found here: https://martin112.gitlab.io/doc3n/doc3n.html
or in the repository under examples/ where there also are some build scripts.

## Requirements

* **python3** main language
* **pandoc** encoding and decoding the markup texts
* **pypandoc** python integration for pandoc
* **magic** Some kind of libmagic ith python integration

## License

LGPL3
