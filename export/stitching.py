#!/usr/bin/python3
"""
Different ways of exporting collected documents.

See each class below or export_lib.py
"""
import logging
from copy import deepcopy
from typing import List, Optional

from .documents import Documents, Header_or_Document, Source
from ..gather import Gather, Cache

from . import pandoc


LOG_STITCH = logging.getLogger("doc3n.stitch")
LOG_TAG = logging.getLogger("doc3n.stitch.tag")


def stitch(
    source: Gather|Cache, documents: Optional[Documents] = None
) -> Documents:
    "Put fragments together to documents with parsed pandoc blocks"
    if not documents:
        documents = Documents()
    if isinstance(source, Gather):
        for fragment in source.get_fragments():
            documents.add_fragment(fragment)
    else:
        for fragment in source.read_fragments():
            documents.add_fragment(fragment)
    documents.try_merge_in_orphans()
    return documents


def source_block(source: Source) -> "pandoc.BLOCK":
    """Create a "_source_" block to fit in pandoc structure, for source
    to be able to fix it later.
    This isn't supported by pandoc, so it has to be handled before exporting
    (this is done in to_page).
    """
    return {"t": "_source_", "c": [str(source.file_path), source.line_nr, source.mime, source.cache]}


def header_to_pandoc_blocks(
    documents: Documents, header: Header_or_Document, level: int, single_page: bool
) -> "List[pandoc.BLOCK]":
    """Join header and its children to a pandoc document"""
    out: "List[pandoc.BLOCK]" = []
    out.append(pandoc.create_header(level, str(header)))

    if "sort_children" in header.tags:
        tag_sort_children(header)

    if "sort_entries" in header.tags:
        tag_sort_paragraphs(header)

    for content in header.content:
        pandoc_blocks: "List[pandoc.BLOCK]" = []
        if content.source and content.pandoc_blocks:
            pandoc_blocks.append(source_block(content.source))
        pandoc_blocks.extend(deepcopy(content.pandoc_blocks))
        out += pandoc_blocks
    for child in header.children:
        out += header_to_pandoc_blocks(
            documents, child, level + 1, single_page=single_page
        )
    return out



def tag_sort_children(header: Header_or_Document) -> None:
    """
    $in:rst tags
    Sort children
    -------------

    Child headers under this header are sorted alphabetically. Example usage::

        $ a doc3n header
        $sort_children:

        B is second
        -----------

        A is first
        ----------
    """
    LOG_TAG.info('sort children under header %s', header)
    header.children.sort(key=lambda child_header: str(child_header).lower())


def tag_sort_paragraphs(header: Header_or_Document) -> None:
    """
    $in:rst tags
    Sort paragraphs
    ---------------
    Tells that paragraphs under the header (separate document fragments) are
    sorted alphabetically. If needed, add an optional value to skip the
    first N paragraphs::

        $sort_paragraphs:[skip first N paragraphs]

        # for example
        $ a doc3n header
        $sort_paragraphs:

        Second paragraph

        First paragraph

    """
    skip_entries: int
    try:
        skip_entries = int(
            header.tags["sort_entries"][0]
        )  # only first tag value is used
    except KeyError:
        # no sort tag
        return
    except IndexError:
        # default to 0 - skip no entries
        return
    except ValueError:
        args = header.tags["sort_entries"]
        LOG_TAG.warning(
            f'sort content under header {header} has wrong argument "{args}"'
            ", skip or use an int to skip entries"
        )
        return

    LOG_TAG.info(
        'sort content under header %s (skipping %d first)', header, skip_entries
    )
    contents_keep_order = header.content[:skip_entries]
    contents_sorted = sorted(
        header.content[skip_entries:],
        key=lambda entry: pandoc.read_text(entry.pandoc_blocks[0]).lower(),
    )
    header.content = contents_keep_order + contents_sorted
