#!/usr/bin/python3

"""
Different ways of exporting collected documents.

$doc:rst Doc3n Generate documents


$in:rst Save Documents

Exporters
=========

There are three different styles of exporters,
*single page*, *multiple pages* and *data*.

Single page tries to export one document.

Multi page exports each document separately.

Data export is for example xml or rdf formats.

Export settings
---------------

"""

from typing import Optional
import logging

from .documents import Document as Document

# make these available for __init__
# try not to change these too much (keep build scripts running or easily fixed)
from .documents import Documents as Documents
from .page import Page as Page
from .pages import Pages as Pages
from .stitching import stitch
from .exporter import Exporter

LOG_EXPORT = logging.getLogger("doc3n.export")


LOST_SECTIONS_DEFAULT_TITLE = "Lost sections"


"""
Settings
    orphans title
    page title
    links prefix suffix etc
    special entries handling

    doc = Documnets(gather)
    doc.toPages()
    doc.toPage()
    doc.to_page(...).export("html")
    doc.to_pages(...).export("...")
"""


def _make_orphans_doc(documents: Documents, title: Optional[str]) -> Optional[Document]:
    lost = documents.make_orphans_document(title_str=title or "------")
    if lost and not title:
        for child in lost.children:
            LOG_EXPORT.warning("Lost section: %s", child.header)
        lost = None
    return lost


def to_page(
    documents: Documents,
    page_title: str,
    lost_sections_title: Optional[str] = LOST_SECTIONS_DEFAULT_TITLE,
) -> Page:
    document = documents.get(page_title)
    if not document:
        LOG_EXPORT.info(
            "List of available documents:\n   %s",
            "\n   ".join([doc.header for doc in documents]),
        )
        raise Exception(
            f"Cannot export single document {page_title}, it is not found"
        )
    lost = _make_orphans_doc(documents, lost_sections_title)
    return Page(documents, document, single_page=True, lost_sections_doc=lost)


def to_pages(
    documents: Documents, lost_sections_title: Optional[str] = LOST_SECTIONS_DEFAULT_TITLE
) -> Pages:
    lost = _make_orphans_doc(documents, lost_sections_title)
    return Pages(documents, lost_sections_doc=lost)
