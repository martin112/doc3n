import logging
from typing import List, Optional

from . import pandoc
from .stitching import header_to_pandoc_blocks
from .documents import Documents, Document, Content

log = logging.getLogger(__name__)


class Page:
    """$in:rst Exporters
    Single page exporter
    --------------------
    Export to single file.

    TODO: remove PDF specifics

    It requires you to specify how the source code
    is reached in relation to the documents. Either directly or a full url
    to file location, like f.ex. ``file:///some/where/source/``.

        pdf_doc = export.PdfFile('my doc', doc3n.DOCUMENTS, web_source_root='../')

    TO save the generated document, you specify output folder and optionally a style sheet::

    pdf_doc.write(output_folder='test/', css='../resources/style1.css')

    write each document in a separate .html file

    :documents: Typically Document.get(document_title)
    :web_page_root: example.com/wiki/
    :web_file_root: example.com/files/
    :only: Optional filter of titles to generate documents for
    :return: list with document information in a dict with 'title', 'web_url', 'file_path'
    """

    def __init__(
        self,
        documents: Documents,
        document: Document,
        lost_sections_doc: Optional[Document] = None,
        single_page: bool = False,
    ) -> None:
        self.documents = documents
        self.document = document
        self.title = document.header
        self.lost_sections_doc = lost_sections_doc
        self.single_page = single_page

    def insert_content(self,
            content: List[pandoc.BLOCK]|pandoc.BLOCK|str,
            position: int = 0) -> None:
        """Add content to page.
           Sets at position from start, 0 is prepend (default).
           Use negative position to count backwards, -1 to append."""
        if isinstance(content, str):
            fixed_content = [pandoc.create_paragraph(pandoc.create_text(content))]
        elif not isinstance(content, list):
            fixed_content = [content]
        else:
            fixed_content = content
        if position < 0:
            position = len(self.document.content) + 1 - position
        self.document.content.insert(position, Content(fixed_content))
