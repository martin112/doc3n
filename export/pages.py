import logging
from typing import List, Union, Optional, Any, Callable

from . import pandoc
from . import Page
from .documents import Documents, Document

log = logging.getLogger(__name__)


class Pages:
    """$in:rst Exporters
    Multi page exporter
    -------------------
    This is a base class for exporting to
    multiple pages. It tries to make it easy
    to create exporters for different formats.
    It puts together documents and fix links,
    in a way so it is easy to modify the result,
    mostly in term of link formatting.
    """

    def __init__(
        self, documents: "Documents", lost_sections_doc: "Optional[Document]" = None
    ):
        self.pages: "List[Page]" = []

        for doc in documents:
            self.pages.append(
                Page(documents, doc, lost_sections_doc=None, single_page=False)
            )

        if lost_sections_doc:
            self.pages.append(
                Page(
                    documents,
                    lost_sections_doc,
                    lost_sections_doc=None,
                    single_page=False,
                )
            )

    def __len__(self) -> int:
        return len(self.pages)

    def mod_pages(self, func: "Callable[[Page], Any]") -> None:
        "Run *func*(page) on each page"
        for page in self.pages:
            func(page)

    def insert_content(self,
            content: "Union[List[pandoc.BLOCK], pandoc.BLOCK, str]",
            position: int = 0) -> None:
        """Add content to each page, see page.insert_content for details"""
        for page in self.pages:
            page.insert_content(content, position)
