"""
$in:rst Doc3n Issues

issue: data export
------------------

A way to export to alternative formats (mirdf/ttl/xml).

mirdf_exporter = DataExporter(page, data_format='mirdf', markup_language='html5)
mirdf_exporter.formatter...
mirdf_exporter.prefix = 'doc3n'
mirdf_exporter.namespace = 'https://gitlab.com/martin112/doc3n/',
mirdf_exporter.source_prefix = 'https://gitlab.com/martin112/doc3n/-/blob/master/',
mirdf_exporter.export(Path('mirdf/doc3n.mirdf'))

"""
from pathlib import Path
from typing import Dict, NewType, Final, List  # , Protocol
import logging

from .documents import Documents, Header, Document
from .pandoc import encode_to_str
from .exporter import Formatter, get_formatter

log = logging.getLogger(__name__)


Prefix = NewType("Prefix", str)
URL = NewType("URL", str)
Namespace = NewType("Namespace", URL)
Term = NewType("Term", str)

class RDF:
    PREFIX: Final[Prefix] = Prefix("rdf")
    NAMESPACE: Final[Namespace] = Namespace(URL("http://www.w3.org/2000/01/rdf#"))
    type: Final[Term] = Term('rdf:type')

class RDFS:
    PREFIX: Final[Prefix] = Prefix("rdfs")
    NAMESPACE: Final[Namespace] = Namespace(URL("http://www.w3.org/2000/01/rdf-schema#"))
    label: Final[Term] = Term('rdfs:label')

class Schema:
    PREFIX: Final[Prefix] = Prefix('schema')
    NAMESPACE: Final[Namespace] = Namespace(URL('http://schema.org/'))
    # resource types
    CreativeWork: Final[Term] = Term('schema:CreativeWork')
    Collection: Final[Term] = Term('schema:Collection')
    TextObject: Final[Term] = Term('schema:TextObject')
    # relations
    isPartOf: Final[Term] = Term('schema:isPartOf')
    hasPart: Final[Term] = Term('schema:hasPart')
    # properties
    encodingFormat: Final[Term] = Term('schema:format')  # mime type
    codeRepository: Final[Term] = Term('schema:codeRepository')
    contentUrl: Final[Term] = Term('schema:contentUrl')  # relative to codeRepository
    headline: Final[Term] = Term('schema:headline')
    text: Final[Term] = Term('schema:text')  # in schema:format
    position: Final[Term] = Term('schema:position')


# ??? CODE = "schema:code"  # url source_prefix: "{file_path}#{line_nr}
# ITEM_LIST = "schema:ItemList"
# LIST_ITEM = "schema:ListItem"
# ITEM_LIST_ELEMENT = "schema:itemListElement"
# https://schema.org/Collection A collection of items, e.g. creative works or products.
# https://schema.org/Dataset
# https://schema.org/DigitalDocument


class DataExporter:
    def __init__(self,
                 documents: Documents,
                 collection_title: str|None = None,
                 prefix: Prefix = Prefix('#'),
                 namespace: Namespace = Namespace(URL('local#')),
                 source_prefix: URL = URL('local#'),
                 # data_format = 'mirdf',
                 markup_language = 'html5',
                ) -> None:
        self.documents = documents
        self.collection_title = collection_title
        # self.data_format = data_format
        self.formatter: Formatter = get_formatter(markup_language)
        self.prefix = prefix
        self.namespace = namespace
        self.source_prefix = source_prefix
        self.extra_args = self.formatter.output_pandoc_extra_args({})
        if '--standalone' in self.extra_args:
            self.extra_args.remove('--standalone')

    def _add_document(self, document: Document, output: 'Mirdf'):
        resource = output.add_document(document.title)
        self._add_content(resource, document, output)
        self._add_children(resource, document.children, output)

    def _add_header(self, parent: Term, header: Header, output: 'Mirdf'):
        resource = output.add_header(parent, str(header))
        self._add_content(resource, header, output)
        for child in header.children:
            self._add_header(resource, child, output)
    
    def _add_children(self, resource: Term, children: List[Header], output: 'Mirdf'):
        for child in children:
            self._add_header(resource, child, output)

    def _add_content(self, resource: Term, parent: Document|Header, output: 'Mirdf'):
        for i, content in enumerate(parent.content, start=1):
            # content.source
            # content.pandoc_blocks
            # fix links
            # self.formatter.output_pandoc()

            formatted, _debug = encode_to_str(
                blocks = content.pandoc_blocks,
                to_format = self.formatter.output_pandoc_format,
                extra_args = self.extra_args,
            )
            if formatted:
                output.add_content(resource, formatted, i)
            else:
                log.warning('Failed to convert content for %s %d', resource, i)

    def export(self, output_path: Path):
        """generate data export to file output_path
        supports only single page exports for now
        """
        output = Mirdf(
            prefix=self.prefix,
            namespace=self.namespace,
            formatter=self.formatter,
            collection_title=self.collection_title,
        )
        for doc in self.documents:
            self._add_document(doc, output)
        output.write_to_file(output_path)



#class DataExporter(Protocol):
#    pass
# XML with schema
# turtle
# CSV
# ...


def sanitize_name(name: str) -> str:
    return name.replace(' ', '').replace('/', '').lower()


class Mirdf:  # (DataExporter)
    def __init__(
            self,
            prefix: Prefix,  # rdf prefix for resources
            namespace: Namespace,  # rdf prefix url
            formatter: Formatter,
            collection_title: str|None=None,
            ) -> None:
        self.prefix = prefix
        self.namespace = namespace
        self.resources: Dict[Term, str] = {}
        self.formatter = formatter
        self.collection: Term|None = None
        if collection_title:
            self.collection = self.__collection(collection_title)

    def __collection(self, title: str) -> Term:
        resource = Term(f'{self.prefix}:{sanitize_name(title)}')
        self.resources[resource] = f"""{resource}
  {RDF.type} {Schema.Collection}
  {Schema.headline} "{title}
"""
        return resource

    # handle resources


    def document_resource(self, title: str) -> Term:
        resource = Term(f'{self.prefix}:{sanitize_name(title)}')
        if resource in self.resources:
            log.warning('Document resource %s already defined', resource)
        return resource

    def header_resource(self, parent: Term, header: str) -> Term:
        resource = Term(f'{parent}—{sanitize_name(header)}')
        if resource in self.resources:
            log.warning('Header resource %s already defined', resource)
        return resource

    def content_resource(self, parent: Term) -> Term:
        for i in range(100):  # adhoc value for max nr of contents under one header
            new_res = f'{parent}_{i:03}'
            if new_res not in self.resources:
                return Term(new_res)
        raise Exception('Resource %s has more than 100 content nodes!' % parent)

    # Adding data

    def add_document(self, title: str) -> Term:
        "Each document gets its own node"
        resource = self.document_resource(title)
        text = f"""{resource}
  {RDF.type} {Schema.CreativeWork}
  {Schema.headline} "{title}
"""
        if self.collection:
            text += f"""  {Schema.isPartOf} {self.collection}
"""
        self.resources[resource] = text
        # schema:contentUrl ???
        # schema:keywords "most common keywords in all text objects
        # schema:dateModified - custom fields?
        return resource

    def add_header(self, parent: Term, header: str) -> Term:
        "Each header gets its own node, there may be multiptle content under each node"
        resource = self.header_resource(parent, header)
        self.resources[resource] = f"""{resource}
  {RDF.type} {Schema.CreativeWork}
  {Schema.isPartOf} {parent}
  {Schema.headline} "{header}
"""
        # schema:keywords "most common keywords under this header
        return resource

    def add_content(self, parent: Term, content: str, position: int) -> Term:
        resource = self.content_resource(parent)
        if '\n' in content:
            content_obj = '\n"' + content.replace('\n', '\n"')
        else:
            content_obj = f' "{content}'
        self.resources[resource] = f"""{resource}
  {RDF.type} {Schema.TextObject}
  {Schema.isPartOf} {parent}
  {Schema.text}{content_obj}
  {Schema.position} "{position}
  {Schema.encodingFormat} "{self.formatter.mime}
"""
        return resource
        # TODO schema:encodingFormat "{mime type of file} -- but will this collide with content?
        # TODO schema:contentUrl prefix:"{file_path}#{line_nr}
        # TODO schema:keywords "tags specific for this content? or gather all in the header?
        # TODO file references - how do we do about them ?

    # Get stringified content
    def write_to_file(self, filepath: Path):
        with open(filepath, 'w') as handle:
            handle.write('# mirdf:version "3\n')
            for _resource, content in sorted(self.resources.items()):
                handle.write(content)
