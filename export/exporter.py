"""
$in:rst Exporters

Export settings
===============

When exporting a page or pages, an exporter is created.

This exporter contains settings regarding the export
and a formatter, which is settings and methods for
how to export the specific format.

This is used by page and pages.

To add a new format to doc3n:

   Add a file in formats/ with the wanted setup. It should
   contain a Formatter class from default, and a registration
   of the format. (see f.ex. html5.py).

It is also possible to add a custom formatter outside of doc3n.

Import the module before the export, and make sure it has
registered the format (it may overwrite the existing format)



Usage::

    page = Page()
    html_export = page.export_format('html5')
    html_export.formatter.header_id_separator = 'ba'
    html_page = Exporter(page, 'html5',
            include_pages_ordered=['a', 'b'])
    html_page.append_referred_documents = True
    html_page.formatter.header_id_separator = '!'
    html_page.export("output.html")

"""
from typing import Optional, List, Dict, Set, Tuple, Literal
from pathlib import Path
import logging
from copy import deepcopy

from .documents import Document, Header_or_Document
from .stitching import header_to_pandoc_blocks
from . import pandoc
from .formats.default import get_formatter, Formatter
# default formats included in doc3n
from .formats import html5 as html5
from .formats import dokuwiki as dokuwiki
from .formats import pdf as pdf

from typing import TYPE_CHECKING
from .page import Page
from .pages import Pages

log = logging.getLogger('doc3n.export.exporter')

class Exporter:

    def __init__(self,
            page_or_pages: 'Page|Pages',
            format: str,
            title: Optional[str] = None,
            include_pages_ordered: Optional[List[str]] = None,
            append_referred_documents: bool = True,  # only for single page
            include_source: bool = True,
            page_prefix: str = '',
            file_prefix: str = '',
            source_prefix: str = '',
            extra_pandoc_arguments: Dict[str, str|None]|None = None,
        ):
        self.page_or_pages = page_or_pages
        self.formatter: Formatter = get_formatter(format)
        self.format = format
        self.title = title
        self.include_pages_ordered: List[str] = []
        if include_pages_ordered:
            for page in include_pages_ordered:
                self.include_pages_ordered.append(page.lower())
        self.append_referred_documents = append_referred_documents
        self.include_source = include_source
        self.extra_pandoc_arguments = extra_pandoc_arguments or {}
        self.page_prefix: str = page_prefix
        self.formatter.file_relative_link_prefix = file_prefix
        self.formatter.source_link_prefix = source_prefix


    def export(self, output_path: Path):
        if isinstance(self.page_or_pages, Pages):
            # TODO check that output path is a folder
            for page in self.page_or_pages.pages:
                assert page.single_page == False
                self.__export_page(
                    page,
                    self.formatter.output_file_path(output_path, page.document),
                    single_page=False)
        else:
            assert self.page_or_pages.single_page == True
            # TODO check if output_path is a file or folder
            # if a folder, run output_file_path on it
            self.__export_page(self.page_or_pages, output_path, single_page=True)


    def __export_page(self, page: 'Page', output_file: Path, single_page: bool) -> None:
        log.debug("Exporting to %s with %s", self.format, self.formatter)
        """
        $in:rst Links and Images

        External links
        ==============
        To get links to other documents, images, etc, you have to know the correct path, either
        absolute or relative to the final document.

        If you write the links as f.ex. ``$file:{path/in/project}``, the ''$file:'' part will be
        replaced at each export with the source_prefix_-setting.

        Links to source code fragments
        ==============================
        When exporting documents, links to where in the documentation source code can be included.
        To set the root path to the source code and files, use source_prefix_ and file_prefix_.
        """

        """
        Below - stich together document and fix links
        It can be result in one of many pages,
        one page alone, or one page with other pages
        added to.

        Task:
            go through all blocks
                => main document blocks (copied from document)
                => referred documents
            while any adding referred docs, pick one new
                go through all blocks - header levels, one down
                    => document blocks
                    => referred docs
                append document blocks to main document blocks
                if using refered docs, add to list of referred docs
                until all referred docs been handled
        """
        process_docs_grow: List[Document] = [page.document]
        for page_name in self.include_pages_ordered:
            document = page.documents.get(page_name)
            if document:
                process_docs_grow.append(document)
            else:
                log.warning('Document not found, skip including page title "%s"', page_name)

        output_blocks: List[pandoc.BLOCK] = []
        for process_doc in process_docs_grow:
            # note, process_docs may grow
            blocks_to_fix = header_to_pandoc_blocks(
                page.documents,
                process_doc,
                level=1 if process_doc == page.document else 2,  # add referred docs one level down
                single_page=single_page
            )
            fixed_blocks, referred_docs = _fix_blocks(
                blocks_to_fix=blocks_to_fix,
                document=process_doc,
                documents=list(page.documents),
                lost_sections_doc=page.lost_sections_doc,
                single_page_doc=single_page,
                exporter=self,
            )
            if single_page and self.append_referred_documents:
                for ref_doc in referred_docs:
                    if ref_doc not in process_docs_grow:
                        # add doc to be included (appended to the blocks list)
                        process_docs_grow.append(ref_doc)
            output_blocks.extend(fixed_blocks)

        self.formatter.output_pandoc(
            output_file,
            self.title or page.title,
            output_blocks,
            self.extra_pandoc_arguments)




def _fix_blocks(
    blocks_to_fix: "List[pandoc.BLOCK]",
    document: Document,
    documents: List[Document],
    lost_sections_doc: Optional[Document],
    single_page_doc: bool,
    exporter: Exporter,
    ) -> Tuple[List[pandoc.BLOCK], Set[Document]]:

    # output blocks, fixed inplace
    blocks: "List[pandoc.BLOCK]" = deepcopy(blocks_to_fix)

    # documents that links in these blocks refer to
    referred_docs: Set[Document] = set()

    prev_source: "Optional[Tuple[str, int, str]]" = None
    prev_block: "Optional[pandoc.BLOCK]" = None
    ix = 0
    while ix < len(blocks):
        block = blocks[ix]
        if block == prev_block:
            log.error("Block loop maybe detected %i %r", ix, block)
        prev_block = block

        if block["t"] == "_source_":
            source_block = blocks.pop(ix)
            # Fix custom source blocks to pandoc blocks
            # {'t': '_source_', 'c': [str(source.file_path), source.line_nr, source.mime]}
            if exporter.include_source and source_block["c"] != prev_source:
                prev_source = source_block["c"]
                ix = exporter.formatter.source_block(source_block, ix, blocks)

        else:

            # fix links urls
            for link_as_block in pandoc.search_type(block, ["Link", "Image"]):
                link_typ: Literal['Link', 'Image'] = 'Link' if link_as_block['t'] == 'Link' else 'Image'
                link: pandoc.INLINE_C = {'t': link_typ, 'c': link_as_block['c']}
                url = pandoc.link_url(link)
                if not url:
                    referred_doc = _fix_link_to_header(
                        document=document,
                        documents=documents,
                        lost_sections_doc=lost_sections_doc,
                        last_source=prev_source,
                        link=link,
                        exporter=exporter,
                        single_page_doc=single_page_doc)
                    if referred_doc:
                        referred_docs.add(referred_doc)
                elif url.startswith("$file:"):
                    exporter.formatter.file_link_fix(link, url)
                # else a defined link - keep as is

            ix += 1

    return blocks, referred_docs


def _fix_link_to_header(
    document: Document,
    documents: List[Document],
    lost_sections_doc: Optional[Document],
    last_source: "Optional[pandoc.BLOCK]",
    link: pandoc.INLINE_C,
    exporter: Exporter,
    single_page_doc: bool,
) -> Optional[Document]:
    """Try to find link text in any document and return url for it

    Will first try current document, then go through the other ones

    Return any referred document"""
    header_link_text = pandoc.link_text(link)

    header_in_included_document: Optional[Document] = None

    # search current document first
    header_in_this_doc = document.find_header(header_link_text)
    if header_in_this_doc:
        header_in_other_doc = None
    else:
        header_in_other_doc = search_header_in_all_documents(
            documents=documents,
            header_text=header_link_text,
            lost_sections_doc=lost_sections_doc,
            )
        if header_in_other_doc:
            if single_page_doc and exporter.append_referred_documents:
                header_in_included_document = header_in_other_doc.document
            elif exporter.include_pages_ordered\
             and header_in_other_doc.document.title.lower() in exporter.include_pages_ordered:
                header_in_included_document = header_in_other_doc.document

    if header_in_this_doc or header_in_included_document:
        # local target link in the same final document
        # in same doc in multi page exports
        # in an included document in single page exports
        url = exporter.formatter.header_target(header_link_text)
        pandoc.link_url(link, url)
        log.debug("Fix link in page %s to %s", header_link_text, url)

    elif not header_in_other_doc:  # already tested: and not header_in_included_document:
        # broken link
        # either not found, or not linked to a document that will be included
        styles = pandoc.link_styles(link)
        styles.append('broken')
        pandoc.link_styles(link, styles)
        if not header_in_other_doc:
            if last_source:  # indicator - mostly correct
                if last_source[1]:
                    src = '(%s#%d)' % (last_source[0], last_source[1])
                else:
                    src = '(%s)' % last_source[0]
            else:
                src = ''
            log.warning(
                    'Fix link missing "%s" from "%s" %s',
                    header_link_text, document, src)
        else:
            log.info(
                'Fix link found "%s" from document "%s" to not-included document %s',
                header_link_text,
                document)

    else:
        # link to doc or header in other doc
        # Only match in multi page exports
        url = exporter.formatter.header_url(
            header=header_in_other_doc, page_prefix=exporter.page_prefix)
        pandoc.link_url(link, url)
        log.debug('Fix link "%s" to document "%s" with url %s',
                    header_link_text, header_in_other_doc, url)

    return header_in_included_document



def search_header_in_all_documents(
    documents: List[Document],  # documents to go through
    lost_sections_doc: Optional[Document],  # also check this document
    header_text: str  # look for this header
    ) -> Optional[Header_or_Document]:
    """Look for a header within all documents and return found document"""
    check_docs = documents + ([lost_sections_doc] if lost_sections_doc else [])
    for doc in check_docs:
        header = doc.find_header(header_text)
        if header:
            return doc
    return None



