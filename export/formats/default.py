"""
Default export settings. overwrite with a file matching the format
it is for.
"""

from typing import Protocol, List, Dict
from pathlib import Path
import re
import logging

from ..documents import Header_or_Document, Document
from .. import pandoc

LOG = logging.getLogger('doc3n.export.formats.default')

class FormatterNotFound(Exception):
    "Failed to find a matching formatter"


##### Helpers for formatter

ABS_URL_RE = re.compile(r'^\w+:')

##### Formatter protocol with defaults - place all format specific settings in this

class Formatter(Protocol):
    """Export format Protocol

    $in:rst Export codebase
    Export formats customization
    ============================
    Default implementations for all format specific settings when exporting
    page or pages to file or files

    It is implemented as a protocol to get typing errors in formatters if
    they doesn't match.

    Formatters can easily be implemented as separate plugins, but the included
    ones are always imported, for type checking reasons.
    """

    """
    Link to a header in a specific document

    page_prefix
    -----------
    For links to pages. Some formats and setups may need a specific prefix. For example
    ``https://example.org/my/doc`` or ``?id=``.
    """
    mime: str = ''

    # TODO check if the default is overwritten if an instance is updated
    header_id_separator: str = '-'

    def header_id(self, header: Header_or_Document|str) -> str:
        return str(header).replace(" ", self.header_id_separator)\
            .replace('.', '_')\
            .replace('/', self.header_id_separator).lower()

    def header_target(self, header: str) -> str:
        return self.link_target_delimiter + self.header_id(header)

    def header_url(self, header: Header_or_Document, page_prefix: str) -> str:
        if isinstance(header, Document):
            LOG.debug(f'header_url Document {header} {self.header_id(header)}')
            return page_prefix + self.header_id(header)
        else:
            LOG.debug(f'header_url {header} {self.header_id(header.document)} {self.header_id(header)}')
            return page_prefix + self.header_id(header.document) + self.header_target(str(header))

    def header_link(self, header: Header_or_Document, page_prefix: str) -> pandoc.INLINE:
        url = self.header_url(header, page_prefix)
        return pandoc.create_link(str(header), url)

    """
    How links looks

    $in:rst Export settings
    line_nr_pattern
    ---------------
    How to format line number to document source code. Default is ``#%d``, for ``html``
    it defaults to ``#line%d``. ``%d`` is replaced with line number.
    """
    link_target_delimiter: str = '#'

    """
    $in:rst export codebase
    Source blocks
    -------------
    Sourceblocks have a custom block structure, (``[ str file_path, int line_nr, str mime ]``), 
    that has to be converted to pandoc compatible block.

    The conversion may need adaption for different formats.

    $in:rst Export settings
    source_prefix
    -------------
    Link to a source file for fragment

    return a paragraph or similar to be put in each place where there is a new fragment,
    mostly under the header.

    The style class of a fragment is ``file {major mime} {minor mime}``.
    """

    """
    $in:rst Export settings
    file_prefix
    -----------
    Prefix to use for links starting with a ``$file:...``. If no prefix is given, the
    links will be relative to the exported output folder.
    """
    file_relative_link_prefix: str = ''

    def file_link_url(self, url: str, prefix: str|None=None) -> str:
        if url.startswith('$file:'):
            url = url[6:]
        if not ABS_URL_RE.match(url):
            if prefix is None:
                prefix = self.file_relative_link_prefix
            url = prefix + url
        return url

    def file_link_fix(self, link: pandoc.INLINE_C, url: str, prefix: str|None=None) -> None:
        fix_text = pandoc.link_text(link)
        if fix_text.startswith('$file:'):
            pandoc.link_text(link, fix_text[6:])
        url_fixed = self.file_link_url(url, prefix)
        pandoc.link_styles(link, pandoc.link_styles(link) + ['external file'])
        pandoc.link_url(link, url_fixed)

    """
    $in:rst Export settings
    source_prefix
    -------------
    For links to documentation source code. If it is not set, the file_prefix_ setting is used.
    """

    source_link_prefix: str|None = None  # use file_rel_link_prefix as default
    source_link_pattern_with_line_nr: str = '{url}#{line_nr}'
    source_link_pattern_without_line_nr: str = '{url}'
    source_name_pattern_with_line_nr: str = '{url} {line_nr}'
    source_name_pattern_without_line_nr: str = '{url}'
    source_link_include_lines_over: int = 15

    def source_link(self, source_url: str, line_nr: int, mime: str) -> pandoc.INLINE:
        if line_nr > self.source_link_include_lines_over:
            # skip if close to beginning of file
            link = self.source_link_pattern_with_line_nr.format(url=source_url, line_nr=line_nr)
            name = self.source_name_pattern_with_line_nr.format(url=source_url, line_nr=line_nr)
        else:
            # skip line number if close to beginning of file
            link = self.source_link_pattern_without_line_nr.format(url=source_url)
            name = self.source_name_pattern_without_line_nr.format(url=source_url)
        url = self.file_link_url(link, self.source_link_prefix)
        return pandoc.create_link(name, url, style=f'source file {mime}')

    def source_block(self,
            source_block: pandoc.BLOCK,
            ix: int,
            blocks: List[pandoc.BLOCK],
            ) -> int:
        """Convert source block (already popped from blocks)
        And insert the new source block on right place"""
        # path, line_nr, mime, context = source_block["c"]
        source_path, line_nr, mime, _context = source_block["c"]
        link = self.source_link(source_path, line_nr, mime)
        blocks.insert(ix, {
            "t": "Plain",
            "c": [link] })
        return ix


    """
    $in:rst Export settings
    File path
    ---------
    Path to where files are stored
    """
    output_pandoc_format: str = ''  # overwrite by given format when instansiating if not set

    output_css: str = ''  # custom stylesheet path - when applicable (html/pdf)

    output_file_suffix: str = '.txt'

    output_pandoc_args: Dict[str, str|None] = {'--standalone': None}

    def output_file_path(self, path: Path, document: Header_or_Document) -> Path:
        return path.joinpath(self.header_id(document)).with_suffix(self.output_file_suffix)

    def output_pandoc_extra_args(self, user_args: Dict[str, str|None]) -> List[str]:
        args_dict = Default.output_pandoc_args.copy()
        args_dict.update(self.output_pandoc_args)
        if self.output_css:
            args_dict['--css'] = self.output_css
        args_dict.update(user_args)
        return [
            f"{name}={value}" if value is not None else f"{name}"
            for name, value in args_dict.items()
        ]

    def output_pandoc(self,
        file_path: "Path",
        title: str,
        blocks: "List[pandoc.BLOCK]",
        user_args: Dict[str, str|None],
    ) -> None:

        # Get all args and convert them to list of str
        # 1. output_pandoc_args is the base
        # 3. add exporter specific args
        # 2. add user set args
        # 4. add extra args

        pandoc.encode_to_file(
            blocks=blocks,
            file_path=file_path,
            to_format=self.output_pandoc_format,
            extra_args=self.output_pandoc_extra_args(user_args),
            title=title,
        )



###########################################
# Administration of the formatters

formatters: Dict[str, type[Formatter]] = {}

def add_formatter(formatter: type[Formatter], match: str) -> None:
    "Register a formatter - use for plugins"
    formatters[match] = formatter


class Default(Formatter):
    """Default formatter using all defaults from Formatter"""

add_formatter(Default, 'default')


def get_formatter(format_: str) -> Formatter:
    "Get a formatter, used by exporter, or raise an exception"
    formatter_class = formatters.get(format_)
    if not formatter_class:
        raise FormatterNotFound(f'No formatter found for "{format_}"')
    formatter = formatter_class()
    if not formatter.output_pandoc_format:
        formatter.output_pandoc_format = format_
    if not formatter.mime:
        formatter.mime = f'text/{format_}'
    return formatter
