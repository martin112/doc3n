
import logging

from .. import pandoc
from .default import Default, ABS_URL_RE, Formatter, add_formatter, pandoc

log = logging.getLogger(__name__)


class Dokuwiki(Formatter):
    """ Exporting to docuwiki

    Dokuwiki has it's own markup.
    Files are saved as {doc}.txt

    A suggestion is to place these generated files under a folder,
    creating a context (links now are {context}:{doc}) and make
    the folder readonly for the web server.
    """
    header_id_separator = '_'
    link_target_delimiter = '#'



    """
    Dokuwiki handles links a bit special.
    [[this>]] is replaced with dokuwiki source (server/doku/installation)

    new_url = 'this>../' + path[6:]
    """
    def file_link_url(self, url: str, prefix: str|None=None) -> str:
        url = Default.file_link_url(self, url, prefix)
        if not ABS_URL_RE.match(url):
            url = "this>" + url
        return url

    """
    Add source as note on first block under header.
    (Headers can not have notes or links)
    """
    def source_block(self, source_block, ix, blocks):
        # source_block is already popped
        source_path, line_nr, mime, _context = source_block["c"]
        link = self.source_link(source_path, line_nr, mime)

        next_block = blocks[ix] if ix < len(blocks) else None
        # content_block_list|None = first_content_block_list_in_container(next_block)
        if not next_block:
            blocks.append(pandoc.create_paragraph([pandoc.create_note([link])]))
            ix += 1
        elif next_block["t"] == "Para":
            next_block["c"].insert(0, pandoc.create_note([link]))
        elif next_block["t"] == "OrderedList":
            next_block["c"][1][0][0]["c"].insert(0, pandoc.create_note([link]))
        elif next_block["t"] == "BulletList":
            bullet_content = next_block["c"][0]
            if bullet_content:
                bullet_content[0]["c"].insert(0, pandoc.create_note([link]))
            else:
                bullet_content.append(pandoc.create_paragraph(link))
        elif next_block["t"] == "DefinitionList":
            def_content = next_block["c"][0]
            if def_content:
                def_content[0].insert(0, pandoc.create_note([link]))
            else:
                def_content.append(pandoc.create_note([link]))
        else:
            blocks.insert(ix, pandoc.create_paragraph([pandoc.create_note([link])]))
            ix += 1
        return ix






    output_file_suffix = '.txt'



add_formatter(Dokuwiki, 'dokuwiki')
