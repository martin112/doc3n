
import logging

from .. import pandoc
from .default import ABS_URL_RE, Formatter, add_formatter, pandoc

log = logging.getLogger(__name__)


class Markdown(Formatter):
    """ Exporting to markdown folders
    """

    output_file_suffix = '.md'



add_formatter(Markdown, 'md')
