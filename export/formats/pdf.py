
# from .. import pandoc
from .default import Formatter, add_formatter


class Pdf(Formatter):
    mime = 'application/pdf'

    output_file_suffix = '.pdf'
    output_pandoc_format = 'html5'  # needed for html middle step
    # output_pandoc_args = {'--pdf-engine': 'wkhtmltopdf'}
    output_pandoc_args = {'--pdf-engine': 'weasyprint'}

    """$in:rst Single page exporter
    Export to single pdf file
    -------------------------

    This exporter creates a PDF-file for a selected document.

    It requires you to specify how the source code
    is reached in relation to the documents. Either directly or a full url
    to file location, like f.ex. file:///some/where/source/.

        pdf_doc = export.PdfFile('my doc', doc3n.DOCUMENTS, web_source_root='../')

    TO save the generated document, you specify output folder and optionally a style sheet::

        pdf_doc.write(output_folder='test/', css='../resources/style1.css')
    """



add_formatter(Pdf, 'pdf')
