
from .default import Document, Formatter, add_formatter


class Html5(Formatter):
    mime = "text/html"

    def header_url(self, header, page_prefix):
        if isinstance(header, Document):
            return page_prefix + self.header_id(header) + ".html"
        else:
            return (
                page_prefix
                + self.header_id(header.document)
                + self.output_file_suffix
                + self.link_target_delimiter
                + self.header_id(header)
            )

    header_id_separator = "-"
    output_file_suffix = ".html"
    output_pandoc_format = "html5"
    output_pandoc_args = {}

    """$in:rst Single page exporter
    Export to single html file
    --------------------------

    This exporter creates a html-file for a selected document.

    It requires you to specify how the source code
    is reached in relation to the documents. Either directly or a full url
    to file location, like f.ex. ``file:///some/where/source/``::

        html_docs = export.HtmlFile('my doc', doc3n.DOCUMENTS, web_source_root='../')

    TO save the generated document, you specify output folder and optionally a style sheet::

        html_docs.write(output_folder='test/', css='../resources/style1.css')

    settings[css] = source to css
    """


add_formatter(Html5, "html")
add_formatter(Html5, "html5")
