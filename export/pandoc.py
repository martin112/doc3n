#!/usr/bin/python3
"""
Definitions related to PANDOC

$in:rst Doc3n Codebase

pandoc tools
------------
To convert between different formats, `Pandoc<https://pandoc.org/>`_ is used.
Doc3n uses pandocs internal json representation for stiching together different
fragments to documents.
"""
import logging
import json
import pypandoc  # type: ignore

LOG_IN = logging.getLogger("doc3n.pandoc.in")
LOG_OUT = logging.getLogger("doc3n.pandoc.out")
LOG_MOD = logging.getLogger("doc3n.pandoc.mod")

"""
Text
[{t: type(style), c?: optional content}]

Blocks (All?) in pandoc looks like this
{ TYPE: Type, CONTENT: any }
"""
from typing import List, Dict, Iterator, Union, Optional, Any, TYPE_CHECKING, Tuple, cast
from pathlib import Path

# from .lib.documents import Document

# TODO copy some definitions from pandoc
# https://hackage.haskell.org/package/pandoc-types-1.22/docs/Text-Pandoc-Definition.html

from typing import Literal, TypedDict
TYPES = Literal[
    "Header",
    "Para",
    "Str",
    "Link",
    "Note",
    "Span",
    "Strong",
    "Emph",
    "Superscript",
    "Subscript",
    "HorizontalRule",
    "Table",
    "Image",
    "BlockQuote",
    "CodeBlock",
    "Code",
    "Plain",
    "BulletList",
    "OrderedList",
    "DefinitionList",
    "Space",
    "SoftBreak",
    "LineBreak",
    "_source_",  # custom pandoc block - must be removed before exporting
]
INLINE_SPACE_TYPES = Literal[
    "Space", "SoftBreak", "LineBreak",
]
INLINE_TYPES = Literal[
    "Str", "Emph", "Underline", "Strong",
    "Strikeout", "Superscript", "Subscript",
    "SmallCaps", "Quoted", "Cite", "Code",
    "Math", "RawInline",
    "Link", "Image", "Note", "Span"]

STYLES = Literal["Strong", "Emph", "Superscript", "Subscript"]
BLOCK = TypedDict("BLOCK", {"t": TYPES, "c": Any})
INLINE_C = TypedDict("INLINE_C", {"t": INLINE_TYPES, "c": Any})
INLINE_SPACE = TypedDict("INLINE_SPACE", {"t": INLINE_SPACE_TYPES})
INLINE = Union[INLINE_C, INLINE_SPACE]
TEXT = List[INLINE]
DOCUMENT = TypedDict(
    "DOCUMENT",
    {
        "meta": Dict[str, Any],
        "blocks": List[BLOCK],
        "pandoc-api-version": List[int],
    },
)

# Normal paragraph:
# {"t":"Para","c":[{"t":"Str","c":"hej"}]}

# Full doc:
# {"blocks":[ ... ], "pandoc-api-version":[1,17,5,1],"meta":{...}}

# Meta:
# "meta":{"title":{"t":"MetaInlines","c":[{"t":"Str","c":"Title!"}]}}}


def search_type(content: "BLOCK", match_types: 'List[TYPES]') -> Iterator["BLOCK"]:
    if isinstance(content, list):
        part: Any
        for part in content:
            yield from search_type(part, match_types)
    else:
        if content["t"] in match_types:
            yield content
        elif content["t"] == "OrderedList":
            # [[1, {'t': 'Decimal'}, {'t': 'Period'}], [[{'t': 'Plain', 'c': ...
            yield from search_type(content["c"][1], match_types)
        elif content["t"] == "Span":
            yield from search_type(content["c"][1], match_types)
        elif content["t"] in (
            "Para",
            "BlockQuote",
            "Plain",
            "BulletList",
            "DefinitionList",
            "Strong",
            "Emph",
            "Superscript",
            "Subscript",
            "Note",
        ):
            # all contain lists of text content
            yield from search_type(content["c"], match_types)
        elif content["t"] in (
            "Str",
            "Space",
            "SoftBreak",
            "Span",
            "Hr",
            "Table",
            "CodeBlock",
            "Code",
            "Header",
        ):
            # skip they should not contain any more sub-content
            # TODO check Table and Header
            pass
        else:
            LOG_MOD.debug("search_type unknown  %s", content["t"])


# {'t': 'Link',
#  'c': [
#           ['', [''], []],  # id, classes, ?
#           [{'c': 'AAA', 't': 'Str'}],  # link text TEXT
#           ['./test_order/title_a_aa_aaa.html5', '']  # url, title
# ]}


def create_link(text: str, url: str, style: str = "") -> "INLINE":
    return {
        "t": "Link",
        "c": [["", [style or ""], []], [{"t": "Str", "c": text}], [url, ""]],
    }


def link_text(link: "INLINE_C", text: Optional[str] = None) -> str:
    if text is not None:
        link["c"][1] = create_text(text)
    return read_text(link["c"][1])


def link_id(link: "INLINE_C", id_: Optional[str] = None) -> str:
    if id_ is not None:
        link["c"][0][0] = id_
    return link["c"][0][0]


def link_styles(link: "INLINE_C", styles: Optional[List[str]] = None) -> "List[str]":
    if styles is not None:
        link["c"][0][1] = styles
    return link["c"][0][1]


def link_url(link: "INLINE_C", url: Optional[str] = None) -> str:
    # Also works for images!
    if url is not None:
        link["c"][2][0] = url
    return link["c"][2][0]


# Image url works the same as link_url, probably by intention
# {u't': u'Image',
#  u'c': [
#    [u'', [], [[u'height', u'0.22222in']]],  # ? ? [settings]
#    [{u't': u'Str', u'c': u'sometext'}],  # alt text or image id, not sure
#    [u'image url', u'']]},  # url, ?


# {u't': u'Header',
#  u'c': [
#           2,  # level
#           [u'aa', [], []],  # id, style, ?
#           [{u'c': u'AA', u't': u'Str'}]  #  text
# ]}


def get_header_id(header: str) -> str:
    return header.lower().replace(" ", "-")


def create_header(
    level: int, content: Union["TEXT", str], styles: Optional[List[str]] = None
) -> "BLOCK":
    """Generate a pandoc json header"""
    if isinstance(content, str):
        header_id = get_header_id(content)
        content_text = create_text(content)
    else:
        content_text = content
        header_id = get_header_id(read_text(content))
    if styles is None:
        styles = []
    return {
        "t": "Header",
        "c": [
            level,
            [header_id, styles, []],
            content_text,
        ],
    }


def modify_header_level(header: "BLOCK", level: int) -> "BLOCK":
    assert level > 0, "Levels must be above 0"
    header["c"][0] = level
    return header


def create_list(pandoc_list_entries: "List[BLOCK]") -> "BLOCK":
    entries = [[entry] for entry in pandoc_list_entries]
    return {
        "t": "BulletList",
        "c": entries,
    }


def create_note(content: "List[INLINE]") -> "INLINE":
    """ footnote t: Note, c: content"""
    # When parsing Text.Pandoc.Definition.Block expected an Object with a tag field where the value is one of [Plain, Para, LineBlock, CodeBlock, RawBlock, BlockQuote, OrderedList, BulletList, DefinitionList, Header, HorizontalRule, Table, Div, Null]
    return {"t": "Note", "c": [{"t": "Para", "c": content}]}


def create_text(text: str) -> "TEXT":
    """make pandoc block out of a text
    Pandoc requires some processing:
    * split text by space
    c[ {t string, c part_of_text}, {t space}, {t string c part_of_text}, ....}
    """
    spaced_parts: "TEXT" = []
    for part in text.split(" "):
        if spaced_parts:  # skip adding space in first loop
            spaced_parts.append({"t": "Space"})
        spaced_parts.append({"t": "Str", "c": part})
    return spaced_parts


def create_paragraph(content: "Union[TEXT, List[BLOCK], BLOCK]") -> "BLOCK":
    """Split text by space and create a paragraph with it as a pandoc list content
    {t paragraph, c[ {t string c part_of_text, t space, t string c part_of_text, ....} ]
    """
    if isinstance(content, list):
        return {
            "t": "Para",
            "c": content,
        }
    elif content["t"] != "Para":
        return {
            "t": "Para",
            "c": [content],
        }
    return content


def style_text(style: "STYLES", content: "TEXT") -> "BLOCK":
    return {
        "t": style,
        "c": content,
    }


def read_text(content: Any) -> str:
    # Assuming correct values in pandoc content
    if isinstance(content, str):
        return content
    if isinstance(content, list):
        part: Any
        string: str = ""
        for part in content:
            string += read_text(part)
        return string
    elif isinstance(content, dict):
        if "t" in content:
            typ: str = content["t"]
            if typ == "Header":
                return read_text(content["c"][2])  # type: ignore
            elif typ == "Str":
                return content["c"]  # type: ignore
            elif typ in ("Space", "SoftBreak"):
                return " "
            elif typ == "Para":
                return read_text(content["c"])
            elif typ == "Link":
                return read_text(content["c"][1])
            elif typ == "DefinitionList":
                return " " + read_text(content["c"])
            LOG_IN.warn("Skipping content %s, unknown pandoc", content)  # type:ignore
    LOG_IN.warn("Skipping content %s, unknown object", content)  # type: ignore
    return ""


def add_title_to_meta(pandoc_meta_block: "Dict[str, Any]", title: str) -> None:
    pandoc_meta_block["pagetitle"] = {
        "t": "MetaInlines",
        "c": [{"t": "Str", "c": title}],
    }


# class EncodeT(json.JSONEncoder):
#    def default(self, obj):
#        if isinstance(obj, T):
#            return obj.value
#        return json.JSONEncoder.default(self, obj)


def rebuild_doc(blocks: List["BLOCK"], title: Optional[str] = None) -> "str":
    if not Parsed.json_version:
        LOG_OUT.error(
            "Pandoc version not set, parse a text with PandocParsed to set it"
        )
        raise Exception("Missing pandoc version")
    LOG_OUT.debug("Pandoc json version %s", Parsed.json_version)
    pandoc_json: DOCUMENT = {
        "blocks": blocks,
        "meta": {},
        "pandoc-api-version": Parsed.json_version,
    }
    if title:
        add_title_to_meta(pandoc_json["meta"], str(title))

    pandoc_json_str = json.dumps(pandoc_json)  # , cls=EncodeT)
    return pandoc_json_str


def encode_to_str(
    blocks: List[BLOCK],
    to_format: str,
    extra_args: List[str],
    title: Optional[str] = None,
) -> Union[ Tuple[str,None], Tuple[str|None,str] ]:
    pandoc_doc = rebuild_doc(blocks, title=title)
    # add extra options convert_text(..., extra_args=['--title=%s'])
    LOG_OUT.debug(f"running pandoc -t {to_format} {' '.join(extra_args)}")
    # debugging json that will be used for the document generation
    try:
        output = pypandoc.convert_text(  # type: ignore
            pandoc_doc,
            format="json",
            to=to_format,
            extra_args=extra_args,
        )
    except Exception as err:
        LOG_OUT.exception('generate output failed, %r', err)
        output = None

    if output and (LOG_OUT.getEffectiveLevel() > logging.DEBUG):
        return output, None
    else:
        debug = json.dumps(json.loads(pandoc_doc), indent=2)
        return output, debug


def encode_to_file(
    blocks: List[BLOCK],
    file_path: Path,
    to_format: str,
    extra_args: List[str],
    title: Optional[str] = None,
) -> None:
    output, debug = encode_to_str(blocks, to_format, extra_args, title)
    """
    $in:rst Logging
    Exporting to file
    -----------------
    Pandoc converts each page to a file.
    
    If this fails, a file containing the intermediate pandoc result is created instead,
    with an extra .pandoc suffix
    """
    if output:
        LOG_OUT.info(f"Writing to {file_path}")
        file_path.write_text(output)
    if debug:
        debug_path = file_path.with_name(file_path.name + '.pandoc')
        LOG_OUT.info('for json debug output, see file %s', debug_path)
        debug_path.write_text(debug)


class Parsed:
    json_version: Optional[List[int]] = None

    def __init__(self, level: int, header: Optional[str] = None) -> None:
        self.level = level
        self.header = header
        self.content: "List[BLOCK]" = []

    def append(self, content: "BLOCK") -> None:
        self.content.append(content)

    def empty(self) -> bool:
        return not bool(self.header or self.content)

    @classmethod
    def parse(cls, string: str, markup: str) -> Iterator["Parsed"]:
        """
        convert a string to pandoc internal json format.
        Returns a list (to be able to merge with other document sections:
            [level, header, content]
        """
        try:
            pandoc_json = json.loads(
                pypandoc.convert_text(string, to="json", format=markup)  # type: ignore
            )
            if TYPE_CHECKING:
                pandoc_json = cast(DOCUMENT, pandoc_json)

        except RuntimeError as err:
            LOG_IN.exception(
                "PanDoc could not read %s string %s...", markup, string[:150]
            )
            raise err
        LOG_IN.debug(str(pandoc_json)[:80])
        if not cls.json_version:
            cls.json_version = pandoc_json["pandoc-api-version"]
            LOG_IN.info("Pandoc json API set to %s", cls.json_version)
        pandoc_json_blocks = pandoc_json["blocks"]

        current = Parsed(0, None)
        for json_block in pandoc_json_blocks:
            if json_block["t"] == "Header":
                if not current.empty():
                    yield current
                current = Parsed(json_block["c"][0], read_text(json_block))
            else:
                current.append(json_block)
        if not current.empty():
            yield current


def test() -> None:
    logging.basicConfig(level=logging.DEBUG)
    parsed = Parsed.parse(
        """
Test parsing some code
    
Some title
==========

test double underscore __ here

Link to same header `Some title`_

* sdf
* sdf

#. one
3. two

""",
        "rst",
    )
    from pprint import pprint

    for p in parsed:
        pprint(p.header)
        pprint(p.content)
        print(encode_to_str(p.content, "dokuwiki", []))


if __name__ == "__main__":
    test()
