#!/usr/bin/python3
"""
Internal data structure for extracted documentation

# doc => under None # main
    => tree
    Section.add(tree)
        add from below, if not found in current tree
            add to orphans {under => section}
        check each orphan against added sections

"""
import logging
from typing import Optional, List, Iterator, Dict

from ..gather.fragment import Fragment, Source
from . import pandoc

LOG_HEADER = logging.getLogger("doc3n.document.header")

TYPE_TAGS = Dict[str, List[str]]


class Content:
    def __init__(self, pandoc_blocks: List["pandoc.BLOCK"], source: Optional[Source] = None):
        self.source: Optional[Source] = source
        self.pandoc_blocks: List["pandoc.BLOCK"] = pandoc_blocks


class Header_or_Document:
    """
    Main header class, use Header or Document instead
    """

    def __init__(self, header: str, document: "Document"):
        self.header: str = header
        self.tags: TYPE_TAGS = {}
        self.document: Document = document
        self.content: List[Content] = []
        self.children: List[Header] = []

    def __str__(self) -> str:
        return self.header

    def find_header(self, header_str: str) -> "Optional[Header_or_Document]":
        if self.match(header_str):
            return self
        for child in self.children:
            child_header = child.find_header(header_str)
            if child_header:
                return child_header
        return None

    def match(self, header: str) -> bool:
        return self.header.lower() == header.lower()

    def add_fragment(self, fragment: Fragment) -> None:
        """
        Parse fragment content and merge it in under this header
        """
        if fragment.tags:  # sometimes tags is an empty list, don't know why
            for tag, values in fragment.tags.items():
                self.tags.setdefault(tag, []).extend(values)
        blocks: Iterator[pandoc.Parsed] = pandoc.Parsed.parse(
            fragment.content, fragment.markup
        )
        block = next(blocks, None)
        if block and not block.header:
            # no header, add text after current header
            LOG_HEADER.debug('adding text under %s', self.header)
            self.content.append(Content(block.content, fragment.source))
            block = next(blocks, None)
        current_level = 0
        current_header: "Header_or_Document" = self
        while block:
            LOG_HEADER.debug('header %s %s', ' ' * current_level, current_header)
            # find or add header
            while block.level <= current_level:
                assert isinstance(
                    current_header, Header
                ), "Trying to move up above doc-level %d %s %s" % (
                    current_level,
                    type(current_header),
                    current_header,
                )
                current_header = (
                    current_header.under_header
                )  # pylint: disable=no-member
                current_level -= 1
            for child_header in current_header.children:
                if child_header.match(str(block.header)):
                    current_header = child_header
                    current_level += 1
                    break
            else:
                new_header = Header(block.header or "", under_header=current_header)
                # for fixing links later on
                self.document.headers.append(str(new_header).lower())
                current_header.children.append(new_header)
                current_header = new_header
                current_level += 1
            # add content
            LOG_HEADER.debug("Add %s under %s", str(block.header), current_header.under_header.header)
            current_header.content.append(Content(block.content, fragment.source))
            block = next(blocks, None)



class Header(Header_or_Document):
    """
    A Header - not top level (Document)
    """

    def __init__(self, header: str, under_header: Header_or_Document):
        Header_or_Document.__init__(self, header, under_header.document)
        self.under_header = under_header  # Header
        # TODO: may need a header level steps down, usually 1 but can be more


class Document(Header_or_Document):
    "A document is a top level header"

    def __init__(self, header: str, documents: "Documents") -> None:
        Header_or_Document.__init__(self, header, self)
        self.title = self.header
        self.headers: List[str] = [header.lower()]
        self.documents = documents

    def has_child_header(self, header_str: str) -> bool:
        return header_str.lower() in self.headers


class Documents:
    """
    search order

    find header
        current working doc reverse
        else orphan

    add header
        any orphan match, insert orphan

    next doc
        match orphans in all previous docs

    find link target
        before and after in doc - order not so important
        in docs after or before - order not so important

    Seems like a Document: [headers] isn't a bad idea

    $in:rst export codebase
    Documents
    ---------

    The class "Documents" handles all documents and fragments.

    It contains a list of "Document" to prepare for export. Each document
    then contain a tree down of headers (in "header" objects). Fragments
    are added to a document by::

        Document.add_fragment(fragment)

    This merges in the fragment with the first matching header under the
    document.
    """

    def __init__(self) -> None:
        self.documents: List[Document] = []
        self.orphans: Dict[str, List[Fragment]] = {}

    def try_merge_in_orphans(self) -> None:
        """Go through orphans, and try to merge in them in the document.
        When secions are added out of order, they end up as orphans.
        Those that cannot be placed anywhere can be added to an orphans
        document with make_orphans_document(self, title_str='Lost sections')"""
        found_headers: "List[str]" = []
        for header_str, fragments in self.orphans.items():
            for doc in self.documents:
                if doc.match(header_str):
                    found_headers.append(header_str.lower())
                    for fragment in fragments:
                        doc.add_fragment(fragment)
                    break
            else:
                for doc in reversed(self.documents):
                    header = doc.find_header(header_str)
                    if header:
                        for fragment in fragments:
                            header.add_fragment(fragment)
                        found_headers.append(header_str.lower())
        for header_str in found_headers:
            if header_str in self.orphans:
                del self.orphans[header_str]

    def __add_orphan(self, fragment: Fragment) -> None:
        assert fragment.main_header_str
        self.orphans.setdefault(fragment.main_header_str.lower(), []).append(fragment)

    def add_fragment(self, fragment: Fragment) -> None:
        """
        for doc-fragments get document or create a new one
        otherwise
            try find header in last added document
            else add fragment to orphans
        if header found, parse fragment and merge in under header
        """
        # Find header to place fragment under
        header: Optional[Header_or_Document] = None
        if fragment.is_root_doc:
            # merge in orphans when we start with a new header,
            # trying to keep fragments close to each other.
            self.try_merge_in_orphans()
            # get document
            header = self.get(fragment.main_header_str)
            # or create it if it doesn't exist
            if not header:
                header = Document(fragment.main_header_str, self)
                self.documents.append(header)
            # add fragment to fetched or created header
            header.add_fragment(fragment)
        else:
            header_str = fragment.main_header_str
            # find header in last added document
            if self.documents and self.documents[-1].has_child_header(header_str):
                header = self.documents[-1].find_header(header_str)
            # if header found add fragment to it, else add fragment to orphans
            if header:
                header.add_fragment(fragment)
            else:
                self.__add_orphan(fragment)

    def __len__(self) -> int:
        return len(self.documents)

    def __iter__(self) -> "Iterator[Document]":
        return iter(self.documents)

    def get(self, title_str: str) -> "Optional[Document]":
        for doc in self:
            if doc.match(title_str):
                return doc
        return None

    def make_orphans_document(
        self, title_str: str = "Lost sections"
    ) -> Optional["Document"]:
        """try merge in orphans, but put those remaining in a separate document and return it"""
        self.try_merge_in_orphans()
        if self.orphans:
            doc = Document(title_str, self)
            for header_str, fragments in self.orphans.items():
                header = Header(header_str + '🗴', under_header=doc)
                doc.children.append(header)
                for fragment in fragments:
                    LOG_HEADER.warning(
                        'Missing header %s for fragment %s',
                        header_str, fragment.source or '')
                    header.add_fragment(fragment)
            return doc
        return None
